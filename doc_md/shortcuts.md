# Shortcuts Cheatsheet

Here is the list of all available shortcuts

| Combination      | Action                                                              |
|------------------|---------------------------------------------------------------------|
| ctrl+O           | Open a pipeline/session                                             |
| ctrl+S           | Save the pipeline                                                   |
| ctrl+E           | Open the form export dialog                                         |
| ctrl+shift+E     | Export the forms directly                                           |
| ctrl+W           | Open the switch workspace dialog                                    |
| ctrl+shift+W     | Open the new workspace dialog                                       |
| ctrl+<arrow_key> | Switch the state of the corresponding panel (left right up or down) |