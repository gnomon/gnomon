# gnomon.utils documentation

## `gnomonPlugin` module

```{eval-rst}
.. automodule:: gnomon.utils.gnomonPlugin
    :members:
```