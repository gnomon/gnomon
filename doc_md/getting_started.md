# Getting started

:::{toctree}
:hidden:
:maxdepth: 2

example/project
example/browser
:::

These tutorials illustrate the basic usage of the Gnomon platform, and are a good way to get familiar with the flow of interactions. We suggest that you follow these different tutorials in the given order.

:::{warning}
This section is under construction! Some information might be missing.
:::

# Overview Of a Gnomon workspace
![Brief Description of  a gnomon Workspace](_static/typical_workspace_descpt.png)



:::{card}
:link: example/project
:link-type: doc
:class-header: sd-fs-4 sd-font-weight-bold

Tutorial \#1 
^^^
Create a new project
:::

:::{card}
:link: example/browser
:link-type: doc
:class-header: sd-fs-4 sd-font-weight-bold

Tutorial \#2
^^^
Load a data file
:::

:::{card}
:class-header: sd-fs-4 sd-font-weight-bold

^^^

:::

:::{card}
:class-header: sd-fs-4 sd-font-weight-bold

^^^

:::

:::{card}
:class-header: sd-fs-4 sd-font-weight-bold

^^^

:::
