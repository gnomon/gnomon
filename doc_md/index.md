---
sd_hide_title: true
---

# Gnomon

::::::{div} landing-title
    :style: "padding: 0.1rem 0.5rem 0.6rem 0; background-size: cover; background-image: url(_static/image_quantification.png), linear-gradient(315deg, var(--sd-color-white) 0%, var(--sd-color-primary) 50%); clip-path: polygon(0px 0px, 100% 0%, 100% 100%, 0% calc(100% - 3rem)); -webkit-clip-path: polygon(0px 0px, 100% 0%, 100% 100%, 0% calc(100% - 3rem));"

::::{grid} 2
    :gutter: 2 3 3 3
    :margin: 4 4 1 2

:::{grid-item}
    :columns: 12 4 4 4

```{image} _static/gnomon_logo.png
:width: 200px
:class: sd-m-auto sd-animate-grow50-rot20
```
:::

:::{grid-item}
    :class: sd-text-white sd-fs-4
    :columns: 12 8 8 8

<h1>Gnomon</h1>

A computational platform to analyze and simulate the development of living forms in 3D.

![version](https://anaconda.org/gnomon/gnomon/badges/version.svg)
![last release](https://anaconda.org/gnomon/gnomon/badges/latest_release_date.svg)
![platforms](https://anaconda.org/gnomon/gnomon/badges/platforms.svg)
![license](https://anaconda.org/gnomon/gnomon/badges/license.svg)

:::
::::

::::::


```{eval-rst}
.. toctree::
    :hidden:
    :maxdepth: 2

    installation
    getting_started
    concepts
    forms
    packages
    shortcuts
    examples
    workspaces
    howto
    run_pipeline
    gnomon_utils
```

---

```{eval-rst}
.. grid:: 2
    :gutter: 3

    .. grid-item-card::
        :class-header: sd-text-white sd-bg-primary sd-fs-5 sd-font-weight-bold
        :class-footer: sd-bg-light

        Installation
        ^^^

        Guidelines for installing the Gnomon platform, from sources or already packaged.
        +++

        .. button-link:: installation.html
            :color: primary
            :expand:
            :outline:
            :click-parent:

            How to install gnomon

    .. grid-item-card::
        :class-header: sd-text-white sd-bg-primary sd-fs-5 sd-font-weight-bold
        :class-footer: sd-bg-light

        Getting started
        ^^^

        Discover the basic functionalities of the Gnomon platform : load data, visualize it and open your first workspace.
        +++

        .. button-link:: getting_started.html
            :color: primary
            :expand:
            :outline:
            :click-parent:

            Gnomon "hello world"


    .. grid-item-card::
        :class-header: sd-text-white sd-bg-primary sd-fs-5 sd-font-weight-bold
        :class-footer: sd-bg-light

        Examples
        ^^^

        Perform simple 3D image analysis tasks such as signal enhancement and cell segmentation.
        +++

        .. button-link:: examples.html
            :color: primary
            :expand:
            :outline:
            :click-parent:

            Tutorials on simple scenarios

    .. grid-item-card::
        :class-header: sd-text-white sd-bg-primary sd-fs-5 sd-font-weight-bold
        :class-footer: sd-bg-light

        Architecture
        ^^^

        Get a better understanding of the generic principles underlying the architecture of the Gnomon platform.
        +++

        .. button-link:: concepts.html
            :color: primary
            :expand:
            :outline:
            :click-parent:

            Learn more about concepts

```
