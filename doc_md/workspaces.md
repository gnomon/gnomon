# Workspaces

:::{toctree}
:maxdepth: 1

workspace/data_browsing
workspace/image_preprocessing
workspace/binarization
workspace/segmentation
workspace/image_registration
workspace/cell_image_filter
workspace/cell_image_quantification
workspace/cell_image_tracking
workspace/point_detection
workspace/point_cloud_quantification
workspace/image_meshing
workspace/mesh_processing
workspace/python_algorithm
workspace/morphonet
:::