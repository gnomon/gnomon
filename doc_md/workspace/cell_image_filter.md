# Cell Image Filter Workspace

The Cell Image Filter Workspace allows to apply filtering operations on a [Cell Image form](../form/cell_image), either to the cell regions or to their properties, to produce a new output Cell Image.

Using the input Cell Image from the left view, a Cell Image will be computed with the filtered cell regions or property values, and displayed in the right view.

:::{note}
Plugins for this workspace can be installed with the following plugin package(s):
```shell script
gnomon-utils package install gnomon_package_tissueimage
```
:::