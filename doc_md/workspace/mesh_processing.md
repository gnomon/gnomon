# Mesh Processing Workspace

The Mesh Processing Workspace allows to apply filters either on the elements or on the attributes of a [Mesh form](../form/mesh), producing a new output Mesh.

Using the input Mesh from the left view, a filtered Mesh will be computed and displayed in the right view. In some cases, the structure of the Mesh wil not be changed, but attribute values will be modified or new attributes will be added.

:::{note}
Plugins for this workspace can be installed with the following plugin package(s):
```shell script
gnomon-utils package install gnomon_package_tissuemesh
```
:::