# Image Preprocessing Workspace

The Image Preprocessing Workspace allows to apply preprocessing steps on the intensity signal of an [Image form](../form/image), producing a new output Image.

:::::{hint}
::::{grid}
:gutter: 1
:margin: 0

:::{grid-item}
:columns: auto  
:child-align: center
You can find a detailed tutorial for the Preprocessing Workspace in the
:::

:::{grid-item}
:columns: auto  
:child-align: center
```{button-link} ../example/image_enhancement.html
:color: primary
:outline:
Image Ehancement Scenario
```
:::
::::
:::::

Using the input Image from the left view, a processed Image will be computed and displayed in the right view. In some cases, a BinaryImage form can be used as a mask for some plugins, if there is one in the input view.

:::{note}
Plugins for this workspace can be installed with the following plugin package(s):
```shell script
gnomon-utils package install gnomon_package_tissueimage
gnomon-utils package install gnomon_package_imageenhancement
```
:::