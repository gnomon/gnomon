import os
import sys
import traceback
import logging
import warnings
import importlib
import inspect
import re
import pickle
import zipfile

from base64 import b64decode, b64encode
from functools import wraps
from typing import Callable
from pathlib import Path
from tempfile import TemporaryDirectory
from json import load, loads, dump
from threading import Event

from pkg_resources import iter_entry_points, resource_filename

from setuptools import findall

import gnomon.core
from gnomon.utils.logCapture import StreamCapture
from dtkcore import dtkCoreParameter

__PLUGINS__ = []
DEBUG = True if os.environ.get('DEBUG') else False

if DEBUG:
    print("Debug Mode Activated")
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

class InterruptProcess(Exception):
    def __init__(self, *args):
        super().__init__(*args)

def get_factory(plugin_group: str):
    return getattr(gnomon.core, f"{plugin_group}_pluginFactory")


def import_plugins(file, excludes=[]):
    file = os.path.dirname(file)
    module = os.path.basename(file)
    path = file.split('/')
    ind = [i for i, n in enumerate(path) if n == 'gnomonplugins'][-1]
    module_path = '.'.join(path[ind:])

    for submodule in os.listdir(file):
        if submodule == '__init__.py' or submodule[-3:] != '.py' or submodule[:-3] in excludes:
            continue
        try:
            __import__(module_path + "." + submodule[:-3], locals(), globals())
        except:
            logging.info(module_path + "." + submodule[:-3])
            traceback.print_exc()


def load_plugin_group(group_name: str):
    """
    Load a plugin group by importing the module of every entry point in the group

    Parameters
    ----------
    group_name: str
        Entry point group
    """
    for entry_point in iter_entry_points(group=group_name, name=None):
        logging.info(f"loading {entry_point.name}: ")
        try:
            entry_point.load()
        except Exception as e:
            logging.info(" --> FAILED to load plugin")
            print(e)


def available_plugins(group_name: str) -> list[str]:
    """
    Return the name of every entry point registered in the group (group_name)

    Parameters
    ----------
    group_name: str
        plugin group

    Returns
    -------
    list[str]
        list of the plugin names in the plugin group (keys of the related factory)
    """
    return [ep.name for ep in iter_entry_points(group=group_name, name=None)]


def plugin_metadata(group_name: str, plugin_name: str) -> dict[str, str]:
    """
    Returns a dict of metadata regarding the plugin and its package.

    Package information:
        - package: the name of the package (in conda for instance as it may differ from the import statement)
        - conda_channel: channel from which to pull this package
    Parameters
    ----------
    group_name: str
        Name of the plugin group (or entry_point group)

    plugin_name: str
        Name of the plugin


    Returns
    -------
    Returns a dictionary of string to string containing metadata regarding the plugin and the package.

    """
    out = {}
    try:
        ep = next(iter_entry_points(group_name, name=plugin_name))
    except StopIteration:
        raise ValueError(f"No entry point found names {plugin_name} in group {group_name}")

    root_module = importlib.import_module(ep.module_name.split(".")[0])
    try:
        out["package"] = root_module.package
        out["conda_channel"] = root_module.conda_channel
    except AttributeError as e:
        print(f"Missing either 'package' or 'conda_channel' from root package {ep.module_name.split('.')[0]}")

    # parse plugin decorator for version, coreversion and name
    plugin_code_path = importlib.util.find_spec(ep.module_name).origin
    with open(plugin_code_path, "r") as plugin_code_file:
        plugin_code = plugin_code_file.readlines()

        for line in plugin_code:
            if re.match("@[A-z]+Plugin", line):
                for field in ['version', 'coreversion', 'name']:
                    regex = "[^A-z]" + field + "[ ]*=[ ]*"
                    if "version" in field:
                        regex += "[\"\']([0-9\.]+)[\"\']"
                    else:
                        regex += "[\"\'](.+)[\"\']"
                    regex += "[ ]*[,\)]"
                    match = re.findall(regex, line)
                    if len(match) > 0:
                        out[field] = match[0]

    # plugin metadata
    *module, resource = ep.module_name.split(".")
    path = resource_filename(".".join(module), resource + ".json")
    if os.path.exists(path):
        with open(path, "r") as file:
            out.update(load(file))
    return out


def default_input_accessors(algo_class, form_class: type) -> tuple[str, str]:
    """
    Returns the default accessors for an input of type form_class from algo_class.

    Requires algo_class to define the static methods:
        defaultGetter(form_class.__name__)

        defaultSetter(form_class.__name__)

    Parameters
    ----------
    algo_class: gnomon.core.gnomonAbstractAlgorithm
        base class of the plugin which must implement two static methods
        `defaultGetter(form_class.__name__)` and `defaultSetter(form_class.__name__)`
    form_class: gnomon.core.gnomonAbstractForm
        type of the form for those accessors

    Returns
    -------

    """
    setter_method = algo_class.defaultSetter(form_class.__name__)
    if not setter_method:
        raise KeyError(f"{algo_class.__name__} does not accept input form {form_class.__name__}")
    getter_method = algo_class.defaultGetter(form_class.__name__)
    if not getter_method:
        raise KeyError(f"{algo_class.__name__} does not accept input form {form_class.__name__}")
    return getter_method, setter_method


def default_output_accessors(algo_class, form_class) -> str:
    """
    Returns the default getter for an output of type form_class from algo_class.

    Requires algo_class to define the static methods:
        defaultOutput(form_class.__name__)

    Parameters
    ----------
    algo_class: gnomon.core.gnomonAbstractAlgorithm
        base class of the plugin which must implement two static methods
        `defaultOutput(form_class.__name__)`
    form_class: gnomon.core.gnomonAbstractForm
        type of the form for those accessors

    Returns
    -------

    """
    bound_method = algo_class.defaultOutput(form_class.__name__)
    if not bound_method:
        raise KeyError(f"{algo_class.__name__} does not accept output form {form_class.__name__}")
    return bound_method


def register_input(cls: type, attribute: str):
    """
    Register attribute of cls as input storage for cleaning later.
    """
    if hasattr(cls, "_input_storage_list"):
        getattr(cls, "_input_storage_list").append(attribute)
    else:
        setattr(cls, "_input_storage_list", [attribute])


def register_output(cls: type, attribute: str):
    """
    Register attribute of cls as output storage for cleaning later.
    """
    if hasattr(cls, "_output_storage_list"):
        getattr(cls, "_output_storage_list").append(attribute)
    else:
        setattr(cls, "_output_storage_list", [attribute])

def register_formDataPlugin(cls: type, attribute: list[object]):
    if hasattr(cls, "formData_list"):
        getattr(cls, "formData_list").extend(attribute)
    else:
        setattr(cls, "formData_list", attribute)

def gnomon_declare_plugins(path: str) -> dict[str, list[str]]:
    """
    Returns the entry_points dict used to declare the plugins in plugin groups.

    Pre-defines setuptools entry_points by parsing every python file in path (recursively)
    scanning for the class name and the base class to generate the correct entry point.

    Parsing as follows: `class [class name](gnomonAbstract[base class])`
    where [base class] is the entry_points group and [class name] the entry_point name.

    The entry_point exposes the module.

    Parameters
    ----------
    path: str
        path where to scan the modules for entry_points (usually package root)

    Returns
    -------
    entry_points dict used to declare the plugins in setuptools
    """
    script = findall(path)
    script = [f for f in script if (f.endswith('.py')) and ('__init__' not in f)]

    module_dict = {}

    path_form = [f.replace('/', '.').replace('src.', '') for f in script]

    for module, file in zip(path_form, script):
        with open(file) as f:
            print("  --> Parsing " + str(file))
            datafile = f.readlines()
            for line in datafile:
                if 'class' in line and 'gnomonAbstract' in line:
                    cls = line.split('gnomonAbstract')[1].split(')')[0]
                    name = line.split('class ')[1].split('(')[0]
                    cls = cls[0].lower() + cls[1:]
                    if cls not in module_dict.keys():
                        module_dict[cls] = []
                    module_dict[cls].append(name + ' = ' + module.replace('.py', ''))

    return module_dict


def gnomonParametric(cls):
    """
    Class decorator: implements methods and special methods related to dtkCoreParameter use.

    Those methods access dtkCoreParameter (cross-parameters) which are stored in the dict attribute
    _parameters mapping keys to dtkCoreParameter.

    Implements:
        special methods __setitem__ and __getitem__ to set and get values to and from parameters

        setParameter(self, parameter_name, parameter_value)
            sets parameter_value to self._parameters[parameter_name]
        setParameters(self, params)
            params is a dict of (parameter_name, parameter_value) and setParameters sets the value
            of each self._parameters[parameter_name] to parameter_value.
            parameter_name must already be a key of self._parameters
        parameters(self)
            returns a copy of _parameters
        parameterDict(self)
            returns a dict of (parameter_name, parameter_value)
        parameterGroups(self)
            used the hidden attribute _parameter_groups to define a map that associates parameter names
            to a group name. By default, the group name "" is assigned to all parameters

    Returns
    -------
    decorated class
    """

    # -----------------------------------------------------
    # Plugin parameters concept
    # -----------------------------------------------------

    def __setitem__(self, key, value):
        self.setParameter(key, value)

    cls.__setitem__ = __setitem__

    def __getitem__(self, key):
        if self._parameters[key].typeName() == "dtkCoreParameterPath":
            return self._parameters[key].path()
        else:
            return self._parameters[key].value()

    cls.__getitem__ = __getitem__

    def setParameter(self, parameter_name, parameter_value):
        if parameter_name in self._parameters:
            if isinstance(parameter_value, dtkCoreParameter):
                self._parameters[parameter_name] = parameter_value
            else:
                self._parameters[parameter_name].setValue(parameter_value)
        else:
            warnings.warn("'" + parameter_name + "' is not a valid parameter")

    cls.setParameter = setParameter

    def setParameters(self, params):
        for (parameter_name, parameter_value) in params.items():
            self.setParameter(parameter_name, parameter_value)

    cls.setParameters = setParameters

    def parameters(self):
        return {k: v for k, v in self._parameters.items()}

    cls.parameters = parameters

    def parameterDict(self):
        return {key: value.value() for key, value in self.parameters().items()}

    cls.parameterDict = parameterDict

    def parameterGroups(self):
        groups = {}
        for parameter_name in self._parameters.keys():
            if not hasattr(self, "_parameter_groups") or parameter_name not in self._parameter_groups.keys():
                groups[parameter_name] = ""
            else:
                groups[parameter_name] = self._parameter_groups[parameter_name]
        return groups

    cls.parameterGroups = parameterGroups

    return cls


def serialize(attr):
    """
    Decorator which implements a 'serialize' and 'deserialize' method.

    'serialize' serializes attribute attr with pickle and then encodes it in base64
    'deserialize' does the inverse operation

    Parameters
    ----------
    attr: str
        name of the attribute that will be serialized

    Returns
    -------
    class with a 'serialize' and 'deserialize' method implemented
    """

    def decorator(cls: type):
        def serialize_func(self: object) -> str:
            return b64encode(pickle.dumps(getattr(self, attr))).decode("ascii")

        def deserialize_func(self: object, serialization: str):
            setattr(self, attr, pickle.loads(b64decode(serialization.encode("ascii"))))

        setattr(cls, "serialize", serialize_func)
        setattr(cls, "deserialize", deserialize_func)
        return cls

    return decorator


def seriesReader(form_attr: str, path_attr: str = "path"):
    """
    Decorator for Reader plugins which enables the use of the series container format.

    Wraps the run method to extract and read the forms from the container when a .zip
    file is selected.

    The decorated plugin must have a run method which can read multiple files
    (string of comma-seperated paths).

    Parameters
    ----------
    form_attr: str
        Name of the form attribute where the form read are stored.
    path_attr: str
        Name of the attribute containing the path to be read.

    Returns
    -------
    Class
        Decorated plugin
    """

    def seriesReaderDecorator(cls: type):
        def run_decorator(f: Callable):
            @wraps(f)
            def run_wrapper(self):
                old_paths = getattr(self, path_attr).split(",")
                path = old_paths[0]
                ext = Path(path).suffix
                # logging.info("======================" + repr(ext) + repr(old_paths))
                if ext == ".zip" and len(old_paths) > 1:
                    raise RuntimeError("When opening a series container expected only one.")
                elif ext != ".zip":
                    return f(self)

                # series handling
                new_paths = []
                time_stamps = []
                with TemporaryDirectory() as tmpdirname:
                    # zip file
                    # logging.info(path)
                    container = zipfile.ZipFile(path, mode="r")
                    if "manifest.json" not in container.namelist():
                        raise RuntimeError("Invalid series container, no manifest.json file found.")
                    manifest = loads(container.read("manifest.json").decode("utf-8"))
                    # extracting misc files
                    if "misc_files" in manifest:
                        for filename in manifest["misc_files"]:
                            container.extract(filename, tmpdirname)
                    # extracting form files
                    for t, filename in manifest["series"].items():
                        new_paths.append(container.extract(filename, tmpdirname))
                        time_stamps.append(t)
                    container.close()
                    # logging.info(new_paths)
                    self.setPath(",".join(new_paths))
                    f(self)
                    # logging.info(getattr(self, form_attr))
                    # setattr(self, form_attr, {t: getattr(self, form_attr)[i] for i, t in enumerate(time_stamps)})
                    # logging.info(getattr(self, "image")())

                self.setPath = old_paths

            return run_wrapper

        setattr(cls, "run", run_decorator(cls.run))

        def preview(self):
            return f"{os.path.splitext(inspect.getfile(cls))[0]}.png"

        setattr(cls, "preview", preview)

        return cls

    return seriesReaderDecorator


def seriesWriter(form_attr: str, path_attr: str = "path"):
    """
    Decorator for Writer plugins which enables the use of the series container format.

    Wraps the run method to write each frame of a form series in a .zip container
    as well as saving the timestamps. Only applies for series of more than one frame (timestamp).

    The decorated plugin must have a run method which can write a file.

    Parameters
    ----------
    form_attr: str
        Name of the form attribute where the form_series written to the disk is stored.
    path_attr: str
        Name of the attribute containing the path where to write.

    Returns
    -------
    Class
        Decorated plugin
    """

    def seriesWriterDecorator(cls: type):
        def writerDecorator(f):
            @wraps(f)
            def run_wrapper(self):
                paths: list[str] = getattr(self, path_attr).split(",")
                path: str = paths[0]
                stem, *extensions = os.path.basename(path).split(".")
                forms = getattr(self, form_attr)
                if len(forms) == 1 and extensions[-1] != "zip":
                    return f(self)
                # writing the series
                with TemporaryDirectory() as tmpdirname:
                    container = zipfile.ZipFile(Path(path).with_suffix(".zip"), "w", compression=zipfile.ZIP_DEFLATED,
                                                compresslevel=5)
                    suffix = "." + ".".join(extensions)
                    ext: str = suffix if suffix != ".zip" else self.extensions()[0]
                    ext = "." + ext if not ext.startswith(".") else ext
                    filename_template = stem + "_t{:.0f}" + ext
                    manifest = {"extension": ext[1:], "name_format": filename_template, "series": {}, "misc_files": []}
                    not_misc_files = os.listdir(tmpdirname)  # ignore as not part of the form written
                    for i, (t, form) in enumerate(forms.items()):
                        filename = filename_template.format(i)
                        manifest["series"][t] = filename
                        filepath = Path(tmpdirname).joinpath(filename)
                        self.setPath(str(filepath))
                        setattr(self, form_attr, {t: form})
                        f(self)
                        container.write(str(filepath), str(filename))
                        not_misc_files.append(filename)  # ignore as already in zip
                    # saving other files written by the plugins
                    files_inventory = os.listdir(tmpdirname)
                    misc_files = set(files_inventory) - set(not_misc_files)
                    for filename in misc_files:
                        manifest["misc_files"].append(filename)
                        filepath = Path(tmpdirname).joinpath(filename)
                        container.write(str(filepath), str(filename))
                    # writing manifest
                    filepath = Path(tmpdirname).joinpath("manifest.json")
                    with open(filepath, "w") as file:
                        dump(manifest, file)
                    container.write(filepath, "manifest.json")

                    container.close()
                    # resetting
                    setattr(self, form_attr, forms)
                    setattr(self, path_attr, paths)

            return run_wrapper

        setattr(cls, "run", writerDecorator(cls.run))
        return cls

    return seriesWriterDecorator


def formDataPlugin(version: str, coreversion: str, data_setter: str, data_getter: str, name: str = "", base_class=None):
    """
    Registers form data plugins to the plugin factory.

    Must be the top decorator as it will wrap every method of the class to suppress errors.
    Error suppression can be deactivated by setting gnomon.utils.gnomonPlugin.DEBUG to True.

    A form data plugin is a class which implements a subclass of gnomon.core.gnomonAbstractFormData

    Parameters
    ----------
    version: str
        Version of the plugin.
    coreversion: str
        Exact version of gnomon to check for API compatibility.
    data_setter: str
        Name of the setter method which sets the data attribute (where the data is internally stored).
        The setter must take only one argument of the type of the data attribute. For instance, if
        the data attribute is a numpy array, then the prototype of the data_setter method must be
        `def data_setter(self, arr: np.ndarray) -> None:`
    data_getter: str
        Name of the getter method which returns a reference to the data attribute.
        The getter takes no arguments and returns (a reference to) the data attribute.
    base_class

    Returns
    -------

    """

    def decorator(cls):
        if not issubclass(cls, gnomon.core.gnomonAbstractFormData):
            raise TypeError(f"Class {cls.__name__} should be a subclass of a gnomonAbstractFormData interface."
                            f" Otherwise try using corePlugin or visualizationPlugin")

        cls.__data_setter = getattr(cls, data_setter)
        cls.__data_getter = getattr(cls, data_getter)
        cls = _gnomonPlugin(version, coreversion, cls, namespace=gnomon.core, name=name, base_class=base_class)
        return cls
 
    return decorator


def algorithmPlugin(version: str, coreversion: str, name: str = "", base_class=None):
    """
    Registers algorithm plugins to the plugin factory.

    Must be the top decorator as it will wrap every method of the class to suppress errors.
    Error suppression can be deactivated by setting gnomon.utils.gnomonPlugin.DEBUG to True.

    A form data plugin is a class which implements a subclass of gnomon.core.gnomonAbstractAlgorithm

    Applies the gnomonParametric decorator:
        Implements methods and special methods related to dtkCoreParameter use.
        Those methods access dtkCoreParameter (cross-parameters) which are stored in the dict attribute
        _parameters mapping keys to dtkCoreParameter.

        Implements:
            special methods __setitem__ and __getitem__ to set and get values to and from parameters

            setParameter(self, parameter_name, parameter_value)
                sets parameter_value to self._parameters[parameter_name]
            setParameters(self, params)
                params is a dict of (parameter_name, parameter_value) and setParameters sets the value
                of each self._parameters[parameter_name] to parameter_value.
                parameter_name must already be a key of self._parameters
            parameters(self)
                returns a copy of _parameters
            parameterDict(self)
                returns a dict of (parameter_name, parameter_value)


    Parameters
    ----------
    version: str
        Version of the plugin.
    coreversion: str
        Exact version of gnomon to check for API compatibility.
    base_class

    Returns
    -------

    """

    def decorator(cls):
        if not issubclass(cls, gnomon.core.gnomonAbstractAlgorithm):
            raise TypeError(f"Class {cls.__name__} should be a subclass of a gnomonAbstractAlgorithm interface."
                            f" Otherwise try using formDataPlugin or visualizationPlugin")
        # setting clearing methods
        if not hasattr(cls, "_input_storage_list"):
            setattr(cls, "_input_storage_list", [])
        if not hasattr(cls, "_output_storage_list"):
            setattr(cls, "_output_storage_list", [])

        def clearInputs(self):
            logging.info(f"Clearing inputs of {cls.__name__}")
            for attr in getattr(cls, "_input_storage_list"):
                setattr(self, attr, {})

        def clearOutputs(self):
            logging.info(f"Clearing outputs of {cls.__name__}")
            for attr in getattr(cls, "_output_storage_list"):
                setattr(self, attr, {})


        def run_decorator(run):
            def run_wrapper(self):
                # clear outputs before run
                self.clearOutputs()
                run(self)

            return run_wrapper

        setattr(cls, "clearInputs", clearInputs)
        setattr(cls, "clearOutputs", clearOutputs)
        setattr(cls, "run", run_decorator(getattr(cls, "run")))

        # other decorators
        cls = gnomonParametric(cls)  # integrating gnomonParametric in wrapper
        cls = _gnomonPlugin(version, coreversion, cls, namespace=gnomon.core, name=name, base_class=base_class)
        return cls

    return decorator


def modelPlugin(version: str, coreversion: str, name: str = "", base_class=None):
    """
    Registers model plugins to the plugin factory.

    Must be the top decorator as it will wrap every method of the class to suppress errors.
    Error suppression can be deactivated by setting gnomon.utils.gnomonPlugin.DEBUG to True.

    Applies the gnomonParametric decorator:
        Implements methods and special methods related to dtkCoreParameter use.
        Those methods access dtkCoreParameter (cross-parameters) which are stored in the dict attribute
        _parameters mapping keys to dtkCoreParameter.

        Implements:
            special methods __setitem__ and __getitem__ to set and get values to and from parameters

            setParameter(self, parameter_name, parameter_value)
                sets parameter_value to self._parameters[parameter_name]
            setParameters(self, params)
                params is a dict of (parameter_name, parameter_value) and setParameters sets the value
                of each self._parameters[parameter_name] to parameter_value.
                parameter_name must already be a key of self._parameters
            parameters(self)
                returns a copy of _parameters
            parameterDict(self)
                returns a dict of (parameter_name, parameter_value)


    Parameters
    ----------
    version: str
        Version of the plugin.
    coreversion: str
        Exact version of gnomon to check for API compatibility.
    base_class

    Returns
    -------

    """

    def decorator(cls):
        if not issubclass(cls, gnomon.core.gnomonAbstractModel):
            raise TypeError(f"Class {cls.__name__} should be a subclass of a gnomonAbstractModel interface."
                            f" Otherwise try using algorithmPlugin or visualizationPlugin")
        # other decorators
        cls = gnomonParametric(cls)  # integrating gnomonParametric in wrapper
        cls = _gnomonPlugin(version, coreversion, cls, namespace=gnomon.core, name=name, base_class=base_class)
        return cls

    return decorator


def visualizationPlugin(version: str, coreversion: str, name="", base_class=None):
    """
    Registers visualization plugins to the plugin factory.

    Must be the top decorator as it will wrap every method of the class to suppress errors.
    Error suppression can be deactivated by setting gnomon.utils.gnomonPlugin.DEBUG to True.

    A form data plugin is a class which implements a subclass of either
    gnomon.visualization.gnomonAbstractVisualization or gnomon.visualization.gnomonAbstractMatplotlibVisualization

    Applies the gnomonParametric decorator:
        Implements methods and special methods related to dtkCoreParameter use.
        Those methods access dtkCoreParameter (cross-parameters) which are stored in the dict attribute
        _parameters mapping keys to dtkCoreParameter.

        Implements:
            special methods __setitem__ and __getitem__ to set and get values to and from parameters

            setParameter(self, parameter_name, parameter_value)
                sets parameter_value to self._parameters[parameter_name]
            setParameters(self, params)
                params is a dict of (parameter_name, parameter_value) and setParameters sets the value
                of each self._parameters[parameter_name] to parameter_value.
                parameter_name must already be a key of self._parameters
            parameters(self)
                returns a copy of _parameters
            parameterDict(self)
                returns a dict of (parameter_name, parameter_value)


    Parameters
    ----------
    version: str
        Version of the plugin.
    coreversion: str
        Exact version of gnomon to check for API compatibility.
    name: str
        Name of the plugin. Used for the UI
    base_class

    Returns
    -------

    """

    def decorator(cls):
        import gnomon.visualization

        if not issubclass(cls, gnomon.visualization.gnomonAbstractVisualization):
            raise TypeError(f"Class {cls.__name__} should be a subclass of a gnomonAbstractVisualization interface."
                            f" Otherwise try using corePlugin or formDataPlugin")
        cls = gnomonParametric(cls)  # integrating gnomonParametric in wrapper
        cls = _gnomonPlugin(version, coreversion, cls, namespace=gnomon.visualization, name=name, base_class=base_class)
        return cls

    return decorator


def _gnomonPlugin(version, coreversion, cls, namespace, name="", base_class=None):
    # -----------------------------------------------------
    # Doc and Version and Name
    # -----------------------------------------------------

    def documentation(self):
        doc = self.__doc__
        if doc is not None:
            # doc = doc.replace("    ","")
            doc = re.split("--+", doc)[0]
            doc = re.split("\n  +[A-z]*\n", doc)[0]
            doc = doc.replace("\n    \n", "\n\n\n")
            doc = doc.replace("\n\n", "\n\n\n")
            doc = doc.replace("\n    ", " ")
            doc = doc.replace("\n ", "\n")
            doc = "\n" + doc + "\n\n"
        else:
            doc = "\nThis plugin has no documentation\n\n"
        return doc

    cls.documentation = documentation

    cls.__version__ = version

    def _version(self):
        return self.__version__

    cls.version = _version

    def _name(self: cls) -> str:
        return name if name else f"PLACEHOLDER: {cls.__name__}"

    cls.name = _name

    # form memory clean
    if not hasattr(cls, "formData_list"):
        setattr(cls, "formData_list", [])

    # -----------------------------------------------------
    # Debugging
    # -----------------------------------------------------
    def destructor_decorator(f):
        def destructor_wrapper(self):
            if DEBUG:
                logging.debug(f"{cls.__name__} is dying")
            if hasattr(cls, "clearInputs"):
                self.clearInputs()
                self.clearOutputs()

            for s in getattr(cls, "formData_list"):
                if DEBUG:
                    logging.debug(f"destroy {s}")
                #s.__swig_destroy__(s)
            self.formData_list.clear()

            f(self)

        return destructor_wrapper

    original_del = getattr(cls, "__del__") if hasattr(cls, "__del__") else lambda self: None
    setattr(cls, "__del__", destructor_decorator(original_del))

    # -----------------------------------------------------
    # TCP Logging
    # -----------------------------------------------------

    def _setLogServerAddress(self, addr: str):
        address, port = addr.split(":")
        self._log_server_address = (address, int(port))

    cls.setLogServerAddress = _setLogServerAddress

    # attach output capture to run method
    if hasattr(cls, "run"):
        _old_run = cls.run

        @wraps(_old_run)
        def logger_init(self, *args, **kwargs):
            # logger init
            _logger = None
            try:
                if hasattr(self, "_log_server_address"):
                    _logger = StreamCapture([sys.stdout, sys.stderr], echo=True, address=self._log_server_address)
                else:
                    _logger = StreamCapture([sys.stdout, sys.stderr], echo=True)
            except Exception as e:
                logging.warning("Could not initialize logger. Server probably not found.")
                pass
            # base run
            out = _old_run(self, *args, **kwargs)
            # cleanup
            if _logger:
                _logger.close()
            return out

        cls.run = logger_init

    # -----------------------------------------------------
    # Progress indicator & interruptions
    # -----------------------------------------------------
    if hasattr(cls, "run"):
        _old_run2 = cls.run

        @wraps(_old_run2)
        def run_init_progress(self, *args, **kwargs):
            self._progress = 0
            self._stop_requested = False
            res = _old_run2(self, *args, **kwargs)
            return res if res else 0  # TODO: change later
        cls.run = run_init_progress

    _old_init = cls.__init__

    @wraps(_old_init)
    def init(self, *args, **kwargs):
        self._stop_requested = False
        self._max_progress = -1
        self._progress_message = ""
        self._event = Event()
        self._event.set()  # release the lock
        self._swigDisownList = []
        return _old_init(self, *args, **kwargs)
    cls.__init__ = init

    def pause(self):
        self._event.clear()

    cls.pause = pause

    def resume(self):
        self._event.set()

    cls.resume = resume

    def stop(self):
        self._stop_requested = True
        self._event.set()
        self._progress = 0

    cls.stop = stop

    def set_max_progress(self, v: int):
        self._max_progress = v

    cls.set_max_progress = set_max_progress

    def increment_progress(self, increase: int = 1, message: str = None):
        """Increment the progress counter by increase and can pause or stop the computation if requested"""
        self._progress += increase
        if message:
            self.set_progress_message(message)
        self._event.wait()
        if self._stop_requested:
            raise InterruptProcess

    cls.increment_progress = increment_progress

    def set_progress_message(self, message: str):
        """Sets the progress message that will be displayed in the progress bar"""
        self._progress_message = message

    cls.set_progress_message = set_progress_message

    def progress(self):
        if self._max_progress <= 0 or self._progress < 0:
            return -1
        else:
            return self._progress*100//self._max_progress

    cls.progress = progress

    def progressMessage(self):
        return self._progress_message

    cls.progressMessage = progressMessage

    # -----------------------------------------------------
    # Python error management
    # -----------------------------------------------------

    def wrapper(f):
        @wraps(f)
        def func(self, *args, **kwargs):
            try:
                return f(self, *args, **kwargs)
            except InterruptProcess:
                # normal interruption
                return 1
            except Exception as e:
                if DEBUG:  # if debug let it throw
                    traceback.print_exc()
                    raise
                traceback.print_exc()
                print(e)
                return 2

        return func

    for key, value in cls.__dict__.items():
        if callable(value) and key not in ["increment_progress"]:
            setattr(cls, key, wrapper(value))

    def pluginName(self):
        return cls.__name__

    setattr(cls, "pluginName", pluginName)

    # -----------------------------------------------------
    # Factory registration
    # -----------------------------------------------------
    if base_class is None:
        base_class = cls.__bases__[0]

    gnomonPluginBaseClass = getattr(namespace, base_class.__name__ + "Plugin")

    def checkVersion(plugin_coreversion: str) -> bool:
        coreversion = ("${gnomon_VERSION}").split('.')
        plugin_coreversion = plugin_coreversion.split('.')

        if int(coreversion[0]) != int(plugin_coreversion[0]):
            return False

        # if minor level of ref < elem return false
        # TODO put < when major > 0
        if int(coreversion[1]) < int(plugin_coreversion[1]):
            return False
        else:
            # no patch level specified in ref
            if len(coreversion) < 3 or len(plugin_coreversion) < 3:
                return True

            # if same minor level, compare patch level
            if (int(coreversion[1]) == int(plugin_coreversion[1])) and (
                    int(coreversion[2]) < int(plugin_coreversion[2])):
                return False
            # else minor level of elem < ref , then don't compare patch level

        return True

    class gnomonPluginClass(gnomonPluginBaseClass):
        def __init__(self):
            super(gnomonPluginClass, self).__init__()
            self.thisown = 0

        def create(self):
            try:
                obj = cls()
                obj.__disown__()
                return obj
            except Exception as e:
                print(e)
                raise e

    __PLUGINS__.append(gnomonPluginClass())

    plugin_factory_name = base_class.__name__.replace("gnomon", "", 1).replace("Abstract", "", 1)
    plugin_factory_name = plugin_factory_name[0].lower() + plugin_factory_name[1:]
    plugin_factory_name += '_pluginFactory'
    factory = getattr(namespace, plugin_factory_name)()

    plugin_key = cls.__name__
    plugin_key = plugin_key[0].lower() + plugin_key[1:]

    # TODO
    # check plugin gnomon_version to actual version before registering it
    # register plugin_version to be able to get it ?
    if checkVersion(coreversion):
        factory.recordPlugin(plugin_key, __PLUGINS__[-1], name, inspect.cleandoc(cls.__doc__) if cls.__doc__ else "")
        if plugin_key in factory.keys():
            logging.info("Python plugin " + str(plugin_key) + ":" + name + " has been successfully loaded!")
    else:
        logging.warn("Python plugin" + str(plugin_key) + "defined for core version " + str(
            coreversion) + " but actual version is ${gnomon_VERSION}")
        logging.warn("plugin not loaded")
    return cls
