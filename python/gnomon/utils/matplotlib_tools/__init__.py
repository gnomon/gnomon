from .backend_qtquickagg import gnomon_figure, FigureCanvas
from .colormap import register_gnomon_colormaps

register_gnomon_colormaps()