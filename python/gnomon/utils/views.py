import vtk
import matplotlib.pyplot as plt

from gnomon.visualization import gnomonAbstractView, gnomonVtkView, gnomonMplView, gnomonQmlView
from gnomon.utils.matplotlib_tools.backend_qtquickagg import manager_instance, MplCanvasZoomDrag


class gnomonStandaloneVtkView(gnomonVtkView):
    """
    Standalone VtkView object to be used outside the Gnomon application

    """

    def __init__(self, parent=None, size=(1000, 1000), offscreen=False):
        super().__init__(parent)

        self._render_window = vtk.vtkRenderWindow()
        if offscreen:
            self._render_window.SetOffscreenRendering(True)
        self._render_window.SetSize(*size)

        self._render_window_interactor = vtk.vtkRenderWindowInteractor()
        self._render_window_interactor.SetRenderWindow(self._render_window)

        self.associate(self._render_window)

        self._render_window_interactor.Initialize()
        self._render_window_interactor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

    def show(self):
        self._render_window_interactor.Start()

    def empty(self):
        return False


class gnomonStandaloneMplView(gnomonMplView):
    """
    Standalone MplView object to be used outside the Gnomon application

    """

    def __init__(self, parent=None, size=(1000, 1000)):
        super().__init__(parent, True)

        num = manager_instance.num
        figsize = [s/plt.rcParams['figure.dpi'] for s in size]
        self._figure = plt.figure(num, figsize=figsize)
        self.setFigureNumber(num)

        manager_instance._canvas[num] = self._figure.canvas
        manager_instance._figures[num] = self._figure
        manager_instance._connects[num] = MplCanvasZoomDrag(self._figure)
        manager_instance._connects[num].connect()
        manager_instance.num += 1

    def render(self):
        self._figure.canvas.draw()

    def clear(self):
        self._figure.clf()
        self._figure.canvas.draw()

    def saveScreenshot(self, filename):
        self._figure.savefig(filename)

    def _update_plot_limits(self):
        self._figure.set_xlim(self.xMin(), self.xMax())
        self._figure.set_xlim(self.yMin(), self.yMax())
        self._figure.canvas.draw()

    def setXMin(self, x_min):
        super().setXMin(x_min)
        self._update_plot_limits()

    def setXMax(self, x_max):
        super().setXMax(x_max)
        self._update_plot_limits()

    def setYMin(self, y_min):
        super().setYMin(y_min)
        self._update_plot_limits()

    def setYMax(self, y_max):
        super().setYMax(y_max)
        self._update_plot_limits()

    def show(self):
        self._figure.show()

    def empty(self):
        return False


class gnomonStandaloneQmlView(gnomonQmlView):
    """
    Standalone QmlView object to be used outside the Gnomon application

    """

    def __init__(self, parent=None, size=(1000, 1000)):
        super().__init__(parent)
        figsize = [s/plt.rcParams['figure.dpi'] for s in size]
        self._figure = plt.figure(figsize=figsize)
        self._figure.patch.set_facecolor("#666666")

    def render(self):
        ax = self._figure.gca()
        ax.text(
            0, 0, self.displayText(), wrap=True,
            color='w', size=self.fontSize(), ha='center', va='center'
        )
        ax.set_xlim(-1, 1)
        ax.set_ylim(-1, 1)
        ax.axis('off')

    def clear(self):
        self._figure.clf()

    def saveScreenshot(self, filename):
        self.render()
        self._figure.savefig(filename)