===
API
===

Core
====

.. toctree::
  :glob:

  api/core/*

Algotithm
~~~~~~~~~

.. toctree::
  :glob:

  api/core/algorithm/*

Form
~~~~~

.. toctree::
  :glob:

  api/core/form/*

Model
~~~~~

.. toctree::
  :glob:

  api/core/model/*

Visualization
=============

.. toctree::
  :glob:

  api/visualization/*
