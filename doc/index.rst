.. Gnomon documentation master file, created by
   sphinx-quickstart on Thu Nov 22 10:12:16 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: substitutions.txt

======
Gnomon
======

.. image:: https://anaconda.org/gnomon/gnomon/badges/version.svg
   :target: https://anaconda.org/gnomon/gnomon

.. image:: https://anaconda.org/gnomon/gnomon/badges/latest_release_date.svg
   :target: https://anaconda.org/gnomon/gnomon

.. image:: https://anaconda.org/gnomon/gnomon/badges/platforms.svg
   :target: https://anaconda.org/gnomon/gnomon

.. image:: https://anaconda.org/gnomon/gnomon/badges/license.svg
   :target: https://anaconda.org/gnomon/gnomon

.. image:: https://anaconda.org/gnomon/gnomon/badges/downloads.svg
   :target: https://anaconda.org/gnomon/gnomon

.. toctree::
   :maxdepth: 2
   :caption: Contents:

|description|

.. sidebar:: **Gnomon**

   :Coordination: |coord|

   :Contributors: |contrib|

   :Active teams:
      * Inria project-team `Mosaic <https://team.inria.fr/mosaic/>`_
      * Inria Sophia `SED <https://www.inria.fr/centre/sophia/innovation/plates-formes-technologiques/>`_

   :Stable release: |release|

   :Written in: C++, Python

   :Supported OS: Linux, Mac OS

   :Licence: `CeCILL-C licence <https://cecill.info/licences/Licence_CeCILL-C_V1-en.html>`_


Installation
============

.. toctree::
   :maxdepth: 2

   installation
   docker

Concepts
========

.. toctree::
   :maxdepth: 2

   concepts

User tutorials
==============

.. toctree::
   :maxdepth: 2

   user-tutorials

Extension tutorials
===================

.. toctree::
   :maxdepth: 2

   extension-tutorials


Indices and tables
==================

.. toctree::
  :maxdepth: 2

  api
* :ref:`genindex`
* :ref:`search`
