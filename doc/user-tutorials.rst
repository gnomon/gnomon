============
Using Gnomon
============


.. toctree::
    :maxdepth: 2

    workspaces/BrowserWorkspace
    workspaces/RegistrationWorkspace
    workspaces/PreprocessingWorkspace
    workspaces/CellDetectionWorkspace
    workspaces/SegmentationWorkspace
    workspaces/CellAnalysisWorkspace
