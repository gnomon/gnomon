#include "gnomonFormVisitor.h"

#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonFormVisitor, formVisitor, gnomonCore);
}

//
// gnomonFormVisitor.cpp ends here
