#include "gnomonAbstractDataDriver.h"
#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractDataDriver, dataDriver, gnomonCore);
}
