#include "gnomonPythonPluginParser.h"

#include <dtkScript>
#include <QtCore>

QString stripQuotes(const QString& str)
{
    QRegularExpression quote_rx("[\'\"](.*)[\'\"]");
    auto match = quote_rx.match(str);
    if (match.hasMatch()) {
        return match.capturedTexts()[1];
    } else {
        return str;
    }
}

QString removeSpaces(const QString& str)
{
    QRegularExpression space_rx("[ ]*(.*)");
    auto match = space_rx.match(str);
    if (match.hasMatch()) {
        return match.capturedTexts()[1];
    } else {
        return str;
    }
}

QString capitalize(const QString& str)
{
    QString cap = str.at(0).toUpper() + str.mid(1);
    return cap;
}

QString unCapitalize(const QString& str)
{
    QString cap = str.at(0).toLower() + str.mid(1);
    return cap;
}

QString argumentValue(const QString& arguments, const QString& argument_name, int argument_position)
{
    QStringList args = arguments.split(",");
    QString arg_value("");
    int arg_pos = 0;
    for (const auto& arg : args) {
        if (arg.contains("=")) {
            QString arg_name = removeSpaces(arg.split("=")[0]);
            if (arg_name == argument_name) {
                arg_value = removeSpaces(arg.split("=")[1]);
            }
        } else if (arg_pos == argument_position) {
            arg_value = arg;
        } else {
            arg_pos += 1;
        }
    }
    return arg_value;
}


// ///////////////////////////////////////////////////////////////////
// gnomonPythonPluginParserPrivate
// ///////////////////////////////////////////////////////////////////


class gnomonPythonPluginParserPrivate: public QObject
{
public:
    QString plugin_class_name;
    QString plugin_name;
    QString plugin_documentation;

public:
    QMap<QString, gnomonFormDescription> input_forms;
    QMap<QString, gnomonFormDescription> output_forms;
    QMap<QString, gnomonParameterDescription> parameters;
    
public:
    QMap<QString, QString> default_data_plugins;
    QMap<QString, QString> parameter_types;
    
public:
     gnomonPythonPluginParserPrivate(void);
    ~gnomonPythonPluginParserPrivate(void);
};

gnomonPythonPluginParserPrivate::gnomonPythonPluginParserPrivate(void)
{
    this->default_data_plugins["gnomonBinaryImage"] = "gnomonBinaryImageDataSpatialImage";
    this->default_data_plugins["gnomonCellComplex"] = "gnomonCellComplexDataPropertyTopomesh";
    this->default_data_plugins["gnomonCellImage"] = "gnomonCellImageDataTissueImage";
    this->default_data_plugins["gnomonImage"] = "gnomonImageDataMultiChannelImage";
    this->default_data_plugins["gnomonMesh"] = "gnomonMeshDataPropertyTopomesh";
    this->default_data_plugins["gnomonPointCloud"] = "gnomonPointCloudDataPandas";

    this->parameter_types["Bool"] = "d_bool";
    this->parameter_types["Int"] = "d_int";
    this->parameter_types["Double"] = "d_real";
    this->parameter_types["String"] = "d_inliststring";
    this->parameter_types["StringList"] = "d_inliststringlist";
}

gnomonPythonPluginParserPrivate::~gnomonPythonPluginParserPrivate(void)
{
}

// ///////////////////////////////////////////////////////////////////
// gnomonPythonPluginParser
// ///////////////////////////////////////////////////////////////////

gnomonPythonPluginParser::gnomonPythonPluginParser(void)
{
    d = new gnomonPythonPluginParserPrivate;
}

gnomonPythonPluginParser::~gnomonPythonPluginParser(void)
{
    delete d;
}

const QString& gnomonPythonPluginParser::pluginClassName(void) const
{
    return d->plugin_class_name;
}

const QString& gnomonPythonPluginParser::pluginName(void) const
{
    return d->plugin_name;
}

const QString& gnomonPythonPluginParser::pluginDocumentation(void) const
{
    return d->plugin_documentation;
}

const QMap<QString, gnomonFormDescription>& gnomonPythonPluginParser::inputForms(void) const
{
    return d->input_forms;
}

const QMap<QString, gnomonFormDescription>& gnomonPythonPluginParser::outputForms(void) const
{
    return d->output_forms;
}

const QMap<QString, gnomonParameterDescription>& gnomonPythonPluginParser::parameters(void) const
{
    return d->parameters;
}

const QMap<QString, QString>& gnomonPythonPluginParser::parameterTypes(void) const
{
    return d->parameter_types;
}

const QMap<QString, QString>& gnomonPythonPluginParser::defaultFormDataPlugins(void) const
{
    return d->default_data_plugins;
}

void gnomonPythonPluginParser::parsePluginCode(const QString& plugin_code)
{
    d->input_forms.clear();
    d->output_forms.clear();
    d->parameters.clear();
    
    QStringList code_lines = plugin_code.split("\n");

    bool in_init = false;
    bool in_docstring = false;
    bool docstring_read = false;

    d->plugin_documentation = "";

    QRegularExpression plugin_rx("@(.*)Plugin[(](.*)[)]");

    QRegularExpression class_rx("class (.*)[(]gnomon");
    QRegularExpression docstring_rx("(\"\"\"|''')(.*)");

    QRegularExpression init_rx("def[ ]*__init__[(]self");
    QRegularExpression method_rx("def.*[(]self");

    QRegularExpression input_rx("@(.*)Input[(](.*)[)]");
    QRegularExpression output_rx("@(.*)Output[(](.*)[)]");
    QRegularExpression parameter_rx("self._parameters\\[(.*)\\][ ]*=[ ]*([\\S]*)[(](.*)[)]");

    QRegularExpression str_rx("[\'\"][^=\'\"\n]*[\'\"]");
    QRegularExpression list_rx("\\[[\'\"][^=\'\"\n]*[\'\"]+\\]");

    for (const auto& line : code_lines) {
        if (init_rx.match(line).hasMatch()) {
            in_init = true;
        } else if (method_rx.match(line).hasMatch()) {
            in_init = false;
        }

        if (in_docstring) {
            if (docstring_rx.match(line).hasMatch()) {
                if (line.startsWith("    ")) {
                    d->plugin_documentation += line.mid(4) + "\n";
                } else {
                    d->plugin_documentation += line + "\n";
                }
            } else {
                in_docstring = false;
                docstring_read = true;
            }
        } else {
            auto match = docstring_rx.match(line);
            if ((match.hasMatch()) & (!docstring_read)) {
                in_docstring = true;
                d->plugin_documentation += match.capturedTexts()[2] + "\n";
            }
        }

        auto match = class_rx.match(line);
        if (match.hasMatch()) {
            d->plugin_class_name = match.capturedTexts()[1];
        }

        match = plugin_rx.match(line);
        if (match.hasMatch()) {
            QString args = match.capturedTexts()[2];
            QString name = stripQuotes(argumentValue(args, "name", 2));
            d->plugin_name = name;
        }

        match = input_rx.match(line);
        if (match.hasMatch()) {
            QString form_type = "gnomon" + capitalize(match.capturedTexts()[1]);
            QString args = match.capturedTexts()[2];
            QString attr_name = stripQuotes(argumentValue(args, "attr", 0));
            QString data_plugin = stripQuotes(argumentValue(args, "data_plugin", 3));
            if (data_plugin == "") {
                data_plugin = d->default_data_plugins[form_type];
            }
            d->input_forms[attr_name] = gnomonFormDescription(attr_name, form_type, data_plugin);
        }

        match = output_rx.match(line);
        if (match.hasMatch()) {
            QString form_type = "gnomon" + capitalize(match.capturedTexts()[1]);
            QString args = match.capturedTexts()[2];
            QString attr_name = stripQuotes(argumentValue(args, "attr", 0));
            QString data_plugin = stripQuotes(argumentValue(args, "data_plugin", 2));
            if (data_plugin == "") {
                data_plugin = d->default_data_plugins[form_type];
            }
            d->output_forms[attr_name] = gnomonFormDescription(attr_name, form_type, data_plugin);
        }

        if (in_init) {
            match = parameter_rx.match(line);
            if (match.hasMatch()) {
                QString parameter_name = "";
                QStringList parameter_matches = match.capturedTexts();
                if (parameter_matches.size() > 1) {
                    parameter_name = stripQuotes(parameter_matches[1]);
                }
                QString parameter_type = "";
                if (parameter_matches.size() > 2) {
                    parameter_type = d->parameter_types.key(parameter_matches[2]);
                }
                QString parameter_doc = "";
                QString parameter_args = "";
                if (parameter_matches.size() > 3) {
                    auto split = stripQuotes(parameter_matches[3]).split(QRegularExpression("[\'\"]")); 
                    if(split.size() > 2)
                        parameter_doc = split[2];
                    if(split.size() > 1 )
                    parameter_args = split[1];
                }
                QStringList param_arguments = parameter_args.split(QRegularExpression(","));
                QString parameter_value = "";
                if (parameter_type == "StringList") {
                    parameter_value = "[]";
                    if (parameter_matches.size() > 3) {
                        auto list_match = list_rx.match(parameter_matches[3]);
                        if (list_match.hasMatch()) {
                            parameter_value = list_match.capturedTexts()[0];
                        }
                    }
                } else {
                    if (param_arguments.size() > 1) {
                        parameter_value = param_arguments[1].simplified();
                    }
                }
                QJsonObject args = QJsonObject();
                if (parameter_matches.size() > 3) {
                    auto str_match = str_rx.match(parameter_matches[3]);
                    if (str_match.hasMatch()) {
                        args["label"] = stripQuotes(str_match.capturedTexts()[0]);
                    } else {
                        if (param_arguments.size() > 0) {
                            args["label"] = param_arguments[0].simplified();
                        }
                    }
                    if (parameter_type == "Double" || parameter_type == "Int") {
                        if (param_arguments.size() > 2) {
                            args["min"] = param_arguments[2].simplified();
                        }
                        if (param_arguments.size() > 3) {
                            args["max"] = param_arguments[3].simplified();
                        }
                        if (parameter_type == "Double") {
                            if (param_arguments.size() > 4) {
                                args["decimals"] = param_arguments[4].simplified();
                            }
                        }
                    }
                    if (parameter_type == "String" || parameter_type == "StringList") {
                        auto list_match = list_rx.match(parameter_matches[3]);
                        if (list_match.hasMatch()) {
                            args["list"] = list_match.capturedTexts()[list_match.lastCapturedIndex()];
                        } else {
                            args["list"] = "[]";
                        }
                    }
                }
                d->parameters[parameter_name] = gnomonParameterDescription(parameter_name, parameter_type, parameter_doc, parameter_value, args);
            }
        }
    }
    if (d->plugin_documentation.startsWith('\n')) {
        d->plugin_documentation.remove(0, 1);
    }
    if (d->plugin_documentation.endsWith('\n')) {
        d->plugin_documentation.remove(d->plugin_documentation.size()-1, 1);
    }
    if (d->plugin_documentation.endsWith('\n')) {
        d->plugin_documentation.remove(d->plugin_documentation.size()-1, 1);
    }
}
