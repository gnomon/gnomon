#include "gnomonAbstractPointCloudWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractPointCloudWriter, pointCloudWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
