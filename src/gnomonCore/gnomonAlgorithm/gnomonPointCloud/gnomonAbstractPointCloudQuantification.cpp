#include "gnomonCore.h"
#include "gnomonAbstractPointCloudQuantification.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractPointCloudQuantification, pointCloudQuantification, gnomonCore);
}


//
// gnomonAbstractPointCloudQuantification.cpp ends here
