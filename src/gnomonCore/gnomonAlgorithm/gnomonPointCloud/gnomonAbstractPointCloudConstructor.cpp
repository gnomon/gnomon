#include "gnomonCore.h"
#include "gnomonAbstractPointCloudConstructor.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractPointCloudConstructor, pointCloudConstructor, gnomonCore);
}


//
// gnomonAbstractPointCloudConstructor.cpp ends here
