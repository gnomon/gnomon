#include "gnomonCore.h"
#include "gnomonAbstractPointCloudFromImage.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractPointCloudFromImage, pointCloudFromImage, gnomonCore);
}


//
// gnomonAbstractPointCloudFromImage.cpp ends here
