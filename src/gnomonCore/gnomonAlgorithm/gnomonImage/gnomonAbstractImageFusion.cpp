#include "gnomonAbstractImageFusion.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractImageFusion, imageFusion, gnomonCore);
}

//
// gnomonAbstractImageFusionReader.cpp ends here
