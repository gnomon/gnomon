#include "gnomonAbstractImageRegistration.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractImageRegistration, imageRegistration, gnomonCore);
}

//
// gnomonAbstractImageRegistrationReader.cpp ends here
