#include "gnomonCore.h"
#include "gnomonAbstractImageConstructor.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractImageConstructor, imageConstructor, gnomonCore);
}


//
// gnomonAbstractImageConstructor.cpp ends here
