#include "gnomonAbstractCellComplexWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexWriter, cellComplexWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
