#include "gnomonCore.h"
#include "gnomonAbstractCellComplexConstructor.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexConstructor, cellComplexConstructor, gnomonCore);
}


//
// gnomonAbstractCellComplexConstructor.cpp ends here
