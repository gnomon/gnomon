#include "gnomonCore.h"
#include "gnomonAbstractCellComplexFromCellImage.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexFromCellImage, cellComplexFromCellImage, gnomonCore);
}


//
// gnomonAbstractCellComplexFromCellImage.cpp ends here
