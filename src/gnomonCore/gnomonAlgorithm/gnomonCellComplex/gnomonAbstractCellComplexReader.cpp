#include "gnomonCore.h"
#include "gnomonAbstractCellComplexReader.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexReader, cellComplexReader, gnomonCore);
}


//
// gnomonAbstractCellComplexReader.cpp ends here
