#include "gnomonAbstractCellComplexAdapter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexAdapter, cellComplexAdapter, gnomonCore);
}

//
// gnomonAbstractCellComplexAdapter.cpp ends here
