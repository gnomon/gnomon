#include "gnomonCore.h"
#include "gnomonAbstractBinaryImageFromImage.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractBinaryImageFromImage, binaryImageFromImage, gnomonCore);
}


//
// gnomonAbstractBinaryImageFromImage.cpp ends here
