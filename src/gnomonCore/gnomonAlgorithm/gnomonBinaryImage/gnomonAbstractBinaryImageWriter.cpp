#include "gnomonAbstractBinaryImageWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractBinaryImageWriter, binaryImageWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
