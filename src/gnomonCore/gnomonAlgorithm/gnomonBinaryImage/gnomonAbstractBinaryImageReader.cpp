#include "gnomonCore.h"
#include "gnomonAbstractBinaryImageReader.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractBinaryImageReader, binaryImageReader, gnomonCore);
}


//
// gnomonAbstractBinaryImageReader.cpp ends here
