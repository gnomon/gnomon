#include "gnomonCore.h"
#include "gnomonAbstractMeshFromImage.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshFromImage, meshFromImage, gnomonCore);
}


//
// gnomonAbstractMeshFromImage.cpp ends here
