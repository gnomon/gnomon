#include "gnomonAbstractMeshReader.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshReader, meshReader, gnomonCore);
}

//
// gnomonAbstractMeshReader.cpp ends here
