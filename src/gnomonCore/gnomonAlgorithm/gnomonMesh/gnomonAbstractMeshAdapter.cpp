#include "gnomonAbstractMeshAdapter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshAdapter, meshAdapter, gnomonCore);
}

//
// gnomonAbstractMeshAdapter.cpp ends here
