#include "gnomonAbstractMeshWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshWriter, meshWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
