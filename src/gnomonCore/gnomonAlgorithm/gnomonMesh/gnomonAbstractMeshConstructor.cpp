#include "gnomonCore.h"
#include "gnomonAbstractMeshConstructor.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshConstructor, meshConstructor, gnomonCore);
}


//
// gnomonAbstractMeshConstructor.cpp ends here
