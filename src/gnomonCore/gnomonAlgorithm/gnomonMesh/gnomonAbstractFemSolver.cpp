#include "gnomonAbstractFemSolver.h"

#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractFemSolver, femSolver, gnomonCore);
}

// gnomonAbstractFemSolver.cpp ends here
