#include "gnomonAbstractLStringAdapter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringAdapter, lStringAdapter, gnomonCore);
}

//
// gnomonAbstractLStringAdapter.cpp ends here
