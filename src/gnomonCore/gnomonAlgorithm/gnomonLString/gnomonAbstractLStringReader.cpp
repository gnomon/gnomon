#include "gnomonAbstractLStringReader.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringReader, lStringReader, gnomonCore);
}

//
// gnomonAbstractLStringReader.cpp ends here
