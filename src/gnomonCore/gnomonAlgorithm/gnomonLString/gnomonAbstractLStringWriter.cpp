#include "gnomonAbstractLStringWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringWriter, lStringWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
