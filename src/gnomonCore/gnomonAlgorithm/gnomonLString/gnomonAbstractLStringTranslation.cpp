#include "gnomonCore.h"
#include "gnomonAbstractLStringTranslation.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringTranslation, lStringTranslation, gnomonCore);
}


//
// gnomonAbstractLStringTranslation.cpp ends here
