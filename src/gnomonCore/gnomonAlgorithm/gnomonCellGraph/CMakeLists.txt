## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

## #################################################################
## Sources
## #################################################################

ADD_GNOMON_SUBDIRECTORY_HEADERS(
    gnomonAbstractCellGraphFromImage
    gnomonAbstractCellGraphFromImage.h)


ADD_GNOMON_SUBDIRECTORY_SOURCES(
    gnomonAbstractCellGraphFromImage.cpp)

######################################################################
### CMakeLists.txt ends here
