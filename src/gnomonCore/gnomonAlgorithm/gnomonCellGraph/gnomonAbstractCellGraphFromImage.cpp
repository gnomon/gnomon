#include "gnomonCore.h"
#include "gnomonAbstractCellGraphFromImage.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellGraphFromImage, cellGraphFromImage, gnomonCore);
}


//
// gnomonAbstractCellGraphFromImage.cpp ends here
