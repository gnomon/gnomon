#include "gnomonAlgorithm/gnomonAbstractFormAlgorithm.h"

#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractFormAlgorithm, formAlgorithm, gnomonCore);
}

//
// gnomonAbstractFormAlgorithm.cpp ends here
