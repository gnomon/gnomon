#include "gnomonCore.h"
#include "gnomonAbstractTreeConstructor.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeConstructor, treeConstructor, gnomonCore);
}


//
// gnomonAbstractTreeConstructor.cpp ends here
