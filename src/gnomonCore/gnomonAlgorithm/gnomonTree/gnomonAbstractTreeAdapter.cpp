#include "gnomonAbstractTreeAdapter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeAdapter, treeAdapter, gnomonCore);
}

//
// gnomonAbstractTreeAdapter.cpp ends here
