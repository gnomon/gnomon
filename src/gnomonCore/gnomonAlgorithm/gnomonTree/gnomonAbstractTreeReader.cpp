#include "gnomonAbstractTreeReader.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeReader, treeReader, gnomonCore);
}

//
// gnomonAbstractTreeReader.cpp ends here
