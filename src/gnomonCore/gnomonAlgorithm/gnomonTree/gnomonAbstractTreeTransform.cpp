#include "gnomonAbstractTreeTransform.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeTransform, treeTransform, gnomonCore);
}

//
// gnomonAbstractTreeTransform.cpp ends here
