#pragma once

#include <gnomonCoreExport>

#include <dtkCore>
#include "gnomonCore/gnomonCorePlugin.h"

#include "gnomonAlgorithm/gnomonAbstractAlgorithm.h"

#include "gnomonForm/gnomonTree/gnomonTree.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class GNOMONCORE_EXPORT gnomonAbstractTreeTransform : public gnomonAbstractAlgorithm
{
public:
    virtual ~gnomonAbstractTreeTransform(void) = default;

public:
    virtual void setInput(std::shared_ptr<gnomonTreeSeries> ) = 0;
    virtual std::shared_ptr<gnomonTreeSeries> input() = 0;

    virtual std::shared_ptr<gnomonTreeSeries> output() = 0;

public:
    static inline QString defaultSetter(QString formName) {
        if(formName == "gnomonTree") {
            return {"setInput"};
        }
        return {};
    };
    static inline QString defaultGetter(QString formName) {
        if(formName == "gnomonTree") {
            return {"input"};
        }
        return {};
    };
    static inline QString defaultOutput(QString formName) {
        if(formName == "gnomonTree") {
            return {"output"};
        }
        return {};
    };
};

// ///////////////////////////////////////////////////////////////////
// Give the concept the plugin machinery
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (gnomonAbstractTreeTransform *)
DTK_DECLARE_PLUGIN        (gnomonAbstractTreeTransform, GNOMONCORE_EXPORT)
GNOMON_DECLARE_PLUGIN_FACTORY(gnomonAbstractTreeTransform, GNOMONCORE_EXPORT, treeTransform, gnomonCore)


//
// gnomonAbstractTreeTransform.h ends here
