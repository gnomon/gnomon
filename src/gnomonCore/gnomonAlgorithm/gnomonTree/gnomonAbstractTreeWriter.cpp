#include "gnomonAbstractTreeWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeWriter, treeWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
