#include "gnomonAbstractTreeFromLString.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeFromLString, treeFromLString, gnomonCore);
}

//
// gnomonAbstractTreeFromLString.cpp ends here
