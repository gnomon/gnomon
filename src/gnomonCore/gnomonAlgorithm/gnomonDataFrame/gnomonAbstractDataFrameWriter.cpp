#include "gnomonAbstractDataFrameWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractDataFrameWriter, dataFrameWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
