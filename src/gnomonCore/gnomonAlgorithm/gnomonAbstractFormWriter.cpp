#include "gnomonAbstractFormWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractFormWriter, formWriter, gnomonCore);
}

//
// gnomonAbstractFormWriter.cpp ends here
