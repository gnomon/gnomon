#include "gnomonCore.h"
#include "gnomonAbstractCellImageFilter.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageFilter, cellImageFilter, gnomonCore);
}


//
// gnomonAbstractCellImageFilter.cpp ends here
