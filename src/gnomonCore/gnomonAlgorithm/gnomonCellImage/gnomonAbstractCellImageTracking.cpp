#include "gnomonCore.h"
#include "gnomonAbstractCellImageTracking.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageTracking, cellImageTracking, gnomonCore);
}


//
// gnomonAbstractCellImageTracking.cpp ends here
