#pragma once

#include <QtCore>

#include <gnomonCoreExport.h> // enables the visibility of the concept

#include <dtkCore>

#include "gnomonCore/gnomonCorePlugin.h"
#include "gnomonAlgorithm/gnomonAbstractAlgorithm.h"

#include "gnomonForm/gnomonCellImage/gnomonCellImage.h"
#include "gnomonForm/gnomonDataFrame/gnomonDataFrame.h"
#include "gnomonForm/gnomonImage/gnomonImage.h"
#include "gnomonForm/gnomonMesh/gnomonMesh.h"

class GNOMONCORE_EXPORT gnomonAbstractCellImageQuantification : public gnomonAbstractAlgorithm
{
    //Inputs
public:
    virtual void setCellImage(std::shared_ptr<gnomonCellImageSeries> cellimage) = 0;
    virtual void setImage(std::shared_ptr<gnomonImageSeries> image) {dtkWarn()<<Q_FUNC_INFO<< "not implemented";};
    virtual void setMesh(std::shared_ptr<gnomonMeshSeries> mesh) {dtkWarn()<<Q_FUNC_INFO<< "not implemented";};

    virtual inline std::shared_ptr<gnomonCellImageSeries> cellImage() = 0;
    virtual inline std::shared_ptr<gnomonImageSeries> image() const {
        dtkWarn()<<Q_FUNC_INFO<<"Not implemented";
        return nullptr;
    };
    virtual inline std::shared_ptr<gnomonMeshSeries> mesh() const {
        dtkWarn()<<Q_FUNC_INFO<<"Not implemented";
        return nullptr;
    };

    // Outputs
public:
    virtual std::shared_ptr<gnomonCellImageSeries> outputCellImage() const = 0;
    virtual std::shared_ptr<gnomonDataFrameSeries> outputDataFrame() const = 0;

public:
    static inline QString defaultSetter(QString formName) {
        if (formName == "gnomonCellImage") {
            return {"setCellImage"};
        } else if (formName == "gnomonImage") {
            return {"setImage"};
        } else if (formName == "gnomonMesh") {
            return {"setMesh"};
        }
        return {};
    };
    static inline QString defaultGetter(QString formName) {
        if (formName == "gnomonCellImage") {
            return {"cellImage"};
        } else if (formName == "gnomonImage") {
            return {"image"};
        } else if (formName == "gnomonMesh") {
            return {"mesh"};
        }
        return {};
    };
    static inline QString defaultOutput(QString formName) {
        if (formName == "gnomonCellImage") {
            return {"outputCellImage"};
        } else if (formName == "gnomonDataFrame") {
            return {"outputDataFrame"};
        }
        return {};
    };
};

DTK_DECLARE_OBJECT(gnomonAbstractCellImageQuantification *)

DTK_DECLARE_PLUGIN(gnomonAbstractCellImageQuantification, GNOMONCORE_EXPORT)
GNOMON_DECLARE_PLUGIN_FACTORY(gnomonAbstractCellImageQuantification, GNOMONCORE_EXPORT, cellImageQuantification, gnomonCore)


//
// gnomonAbstractCellImageQuantification.h ends here
