#include "gnomonCore.h"
#include "gnomonAbstractCellImageConstructor.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageConstructor, cellImageConstructor, gnomonCore);
}


//
// gnomonAbstractCellImageConstructor.cpp ends here
