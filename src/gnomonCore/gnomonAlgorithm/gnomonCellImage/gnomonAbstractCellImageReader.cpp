#include "gnomonAbstractCellImageReader.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageReader, cellImageReader, gnomonCore);
}

//
// gnomonAbstractCellImageReader.cpp ends here
