#include "gnomonAbstractCellImageWriter.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageWriter, cellImageWriter, gnomonCore);
}

//
// gnomonAbstractImageWriter.cpp ends here
