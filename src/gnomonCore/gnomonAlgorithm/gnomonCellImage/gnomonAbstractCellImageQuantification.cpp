#include "gnomonCore.h"
#include "gnomonAbstractCellImageQuantification.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageQuantification, cellImageQuantification, gnomonCore);
}


//
// gnomonAbstractCellImageQuantification.cpp ends here
