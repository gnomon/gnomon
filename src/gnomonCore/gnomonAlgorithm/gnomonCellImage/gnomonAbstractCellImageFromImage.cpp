#include "gnomonCore.h"
#include "gnomonAbstractCellImageFromImage.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageFromImage, cellImageFromImage, gnomonCore);
}

//
// gnomonAbstractCellImageFromImage.cpp ends here
