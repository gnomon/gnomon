#pragma once

#include <QtCore>

#include <dtkCore>
#include "gnomonCore/gnomonCorePlugin.h"

#include <gnomonCoreExport>
#include "gnomonForm/gnomonAbstractFormData"

class gnomonTree;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class GNOMONCORE_EXPORT gnomonAbstractTreeData: public gnomonAbstractFormData
{
public:
             gnomonAbstractTreeData(void) = default;
    virtual ~gnomonAbstractTreeData(void) = default;

    virtual gnomonAbstractTreeData* clone(void) const = 0;

    virtual void fromGnomonForm(gnomonAbstractForm* other) = 0;


//  ///////////////////////////////////////////////////////////////////
//  Metadata
//  ///////////////////////////////////////////////////////////////////
public:
    virtual QMap<QString,QString> metadata(void) const = 0;
    virtual QString dataName(void) const = 0;
    virtual const QString pluginName(void) = 0;

public:
    virtual QList<long> vertexIds(void) const = 0;
    virtual long vertexCount(void) const = 0;

    virtual long rootId(void) const = 0;

public:
    virtual bool hasChildren(long vertexId) const = 0;
    virtual QList<long> childrenIds(long vertexId) const = 0;
    virtual long childrenCount(long vertexId) const = 0;

    virtual bool hasParent(long vertexId) const = 0;
    virtual long parentId(long vertexId) const = 0;

    virtual QStringList vertexPropertyNames(void) const = 0;
    virtual bool hasVertexProperty(const QString& propertyName) const = 0;
    virtual QMap<long, QVariant> vertexProperty(const QString& propertyName) const = 0;

    virtual bool isIsomorphicTo(gnomonTree * other) const = 0;
};

// ///////////////////////////////////////////////////////////////////
// Give the concept the plugin machinery
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (gnomonAbstractTreeData *)
DTK_DECLARE_PLUGIN        (gnomonAbstractTreeData, GNOMONCORE_EXPORT)
GNOMON_DECLARE_PLUGIN_FACTORY(gnomonAbstractTreeData, GNOMONCORE_EXPORT, treeData, gnomonCore)


// gnomonAbstractTreeData.h ends here
