## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:


## #################################################################
## Sources
## #################################################################


ADD_GNOMON_SUBDIRECTORY_HEADERS(
  gnomonAbstractTreeData
  gnomonAbstractTreeData.h
  gnomonTree
  gnomonTree.h)


ADD_GNOMON_SUBDIRECTORY_SOURCES(
  gnomonAbstractTreeData.cpp)


######################################################################
### CMakeLists.txt ends here
