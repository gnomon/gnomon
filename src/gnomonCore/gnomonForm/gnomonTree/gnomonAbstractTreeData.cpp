#include "gnomonCore.h"
#include "gnomonAbstractTreeData.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeData, treeData, gnomonCore);
}

// gnomonAbstractTreeData.cpp ends here
