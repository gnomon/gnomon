#include "gnomonCore.h"
#include "gnomonAbstractCellGraphData.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellGraphData, cellGraphData, gnomonCore);
}

//
// gnomonAbstractCellGraphData.cpp ends here
