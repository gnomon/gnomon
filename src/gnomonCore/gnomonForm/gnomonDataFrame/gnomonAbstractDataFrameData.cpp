#include "gnomonCore.h"
#include "gnomonAbstractDataFrameData.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractDataFrameData, dataFrameData, gnomonCore);
}

//
// gnomonDataFrameData.cpp ends here
