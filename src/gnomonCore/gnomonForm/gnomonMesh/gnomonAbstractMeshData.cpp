#include "gnomonCore.h"
#include "gnomonAbstractMeshData.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshData, meshData, gnomonCore);
}

//
// gnomonAbstractMeshData.cpp ends here
