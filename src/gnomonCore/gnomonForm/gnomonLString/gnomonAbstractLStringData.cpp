#include "gnomonCore.h"
#include "gnomonAbstractLStringData.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringData, lStringData, gnomonCore);
}

// gnomonAbstractLStringData.cpp ends here
