#include "gnomonCore.h"
#include "gnomonAbstractDataDictData.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractDataDictData, dataDictData, gnomonCore);
}
