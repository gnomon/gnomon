## #################################################################
## Sources
## #################################################################


ADD_GNOMON_SUBDIRECTORY_HEADERS(
        gnomonBinaryImage
        gnomonBinaryImage.h
        gnomonAbstractBinaryImageData
        gnomonAbstractBinaryImageData.h)


ADD_GNOMON_SUBDIRECTORY_SOURCES(
        gnomonAbstractBinaryImageData.cpp
        )


######################################################################
### CMakeLists.txt ends here
