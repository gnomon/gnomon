#include "gnomonCore.h"
#include "gnomonAbstractCellImageData.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageData, cellImageData, gnomonCore);
}

// gnomonAbstractCellImageData.cpp ends here
