#include "gnomonCore.h"
#include "gnomonAbstractCellComplexData.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexData, cellComplexData, gnomonCore);
}

//
// gnomonCellComplexData.cpp ends here
