#include "gnomonCore.h"
#include "gnomonAbstractImageData.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractImageData, imageData, gnomonCore);
}

//
// gnomonImageData.cpp ends here
