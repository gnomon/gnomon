## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

## ###################################################################
## Sources
## ###################################################################

ADD_GNOMON_SUBDIRECTORY_HEADERS(
  gnomonAbstractIntensityImageData
  gnomonAbstractIntensityImageData.h
  gnomonIntensityImage
  gnomonIntensityImage.h)

ADD_GNOMON_SUBDIRECTORY_SOURCES(
  gnomonAbstractIntensityImageData.cpp)

######################################################################
### CMakeLists.txt ends here
