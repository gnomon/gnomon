#include "gnomonCore.h"
#include "gnomonAbstractIntensityImageData.h"

#include "gnomonCore.h"

// /////////////////////////////////////////////////////////////////
// Register to gnomonCore layer
// /////////////////////////////////////////////////////////////////

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractIntensityImageData, intensityimagedata, gnomonCore);
}

//
// gnomonIntensityImageData.cpp ends here
