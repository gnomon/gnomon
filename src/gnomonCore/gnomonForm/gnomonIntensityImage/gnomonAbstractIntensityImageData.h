#pragma once

class vtkImageData;

#include <QtCore>

#include <dtkCore>
#include "gnomonCore/gnomonCorePlugin.h"

#include <gnomonCoreExport.h>
#include "gnomonForm/gnomonAbstractFormData"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class GNOMONCORE_EXPORT gnomonAbstractIntensityImageData: public gnomonAbstractFormData
{
public:
             gnomonAbstractIntensityImageData(void) = default;
    virtual ~gnomonAbstractIntensityImageData(void) {};

    virtual gnomonAbstractIntensityImageData* clone(void) const = 0;

    virtual void fromGnomonForm(gnomonAbstractForm* other) = 0;


//  ///////////////////////////////////////////////////////////////////
//  Metadata
//  ///////////////////////////////////////////////////////////////////
public:
    virtual QMap<QString,QString> metadata(void) const = 0;
    virtual QString dataName(void) const = 0;
    virtual const QString pluginName(void) = 0;

public:
    virtual void setImage(vtkImageData *image) = 0;
    virtual vtkImageData* image(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////
// Give the concept the plugin machinery
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (gnomonAbstractIntensityImageData *)
DTK_DECLARE_PLUGIN        (gnomonAbstractIntensityImageData, GNOMONCORE_EXPORT)
GNOMON_DECLARE_PLUGIN_FACTORY(gnomonAbstractIntensityImageData, GNOMONCORE_EXPORT, intensityimagedata, gnomonCore)


// gnomonAbstractIntensityImageData.h ends here
