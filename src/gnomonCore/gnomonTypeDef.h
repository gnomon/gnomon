#pragma once

#include <QtCore>

#include <array>
#include <functional>

namespace gnomon {
    typedef std::array<double, 3> vec3_t;
}
//
// macularTypedef.h ends here
