#include "gnomonFemSolverCommand.h"

#include <gnomonCore/gnomonAlgorithm/gnomonMesh/gnomonAbstractFemSolver.h>
#include <gnomonCore/gnomonPythonPluginLoader.h>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class gnomonFemSolverCommandPrivate
{
public:
    QVariantMap parameters;

public:
    std::shared_ptr<gnomonMeshSeries> mesh = nullptr;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

gnomonFemSolverCommand::gnomonFemSolverCommand() : d(new gnomonFemSolverCommandPrivate)
{
    GNOMON_COMMANDS_INIT(gnomonFemSolverCommand)

    QStringList keys = gnomonCore::femSolver::pluginFactory().keys();
    if (!keys.empty()) {
        this->algorithm_name = keys[0];
        this->action = gnomonCore::femSolver::pluginFactory().create(this->algorithm_name);
    }
}

gnomonFemSolverCommand::~gnomonFemSolverCommand()
{
    delete d;
}

void gnomonFemSolverCommand::setAlgorithmName(const QString& algo_name)
{
    this->algorithm_name = algo_name;

        delete this->action;
    this->action = gnomonCore::femSolver::pluginFactory().create(algo_name);
}

void gnomonFemSolverCommand::predo(void)
{
    if (d->mesh) {
        d->mesh->load();
    }
    ((gnomonAbstractFemSolver *) this->action)->setInputMesh(d->mesh);
}

void gnomonFemSolverCommand::postdo(void)
{

}

void gnomonFemSolverCommand::undo()
{
    ((gnomonAbstractFemSolver *) this->action)->setInputMesh(nullptr);
}

void gnomonFemSolverCommand::setMesh(std::shared_ptr<gnomonMeshSeries> mesh)
{
    d->mesh = mesh;
}

std::shared_ptr<gnomonMeshSeries> gnomonFemSolverCommand::updatedMesh()
{
    return ((gnomonAbstractFemSolver *) this->action)->updatedMesh();
}

QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > gnomonFemSolverCommand::inputs() {
    QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm>> inputs;
    inputs["inputMesh"] = this->inputMesh();
    return inputs;
}

gnomonAbstractCommand::orderedMap gnomonFemSolverCommand::inputTypes() {
    orderedMap input_types;
    input_types.emplace_back(std::make_pair("inputMesh", "gnomonMesh"));
    return input_types;
}

QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > gnomonFemSolverCommand::outputs() {
    QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > outputs;
    outputs["updatedMesh"] = this->updatedMesh();
    return outputs;
}

gnomonAbstractCommand::orderedMap gnomonFemSolverCommand::outputTypes() {
    orderedMap output_types;
    output_types.emplace_back(std::make_pair("updatedMesh", "gnomonMesh"));
    return output_types;
}

std::shared_ptr<gnomonMeshSeries> gnomonFemSolverCommand::inputMesh() {
    return this->d->mesh;
}

void gnomonFemSolverCommand::setInputForm(const QString &name, std::shared_ptr<gnomonAbstractDynamicForm> form) {
    if (name == "inputMesh") {
        this->setMesh(std::dynamic_pointer_cast<gnomonMeshSeries>(form));
    } else {
        dtkWarn()<<Q_FUNC_INFO<<"Unknown input "<< name;
    }
}

void gnomonFemSolverCommand::deserializeResults(QJsonObject &serialization) {
    if(!d->mesh) {
        d->mesh = std::make_shared<gnomonMeshSeries>();
    }
    auto tmp = serialization["updatedMesh"].toObject();
    d->mesh->deserialize(tmp);
}

QJsonObject gnomonFemSolverCommand::serializeResults(void) {
    QJsonObject out;
    out["updatedMesh"] = d->mesh->serialize();
    return out;
}

void gnomonFemSolverCommand::clear(void) {
    gnomonAbstractAlgorithmCommand::clear();
    d->mesh = nullptr;
}

//
// gnomonFemSolverCommand.cpp ends here
