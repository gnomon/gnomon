#include "gnomonPointCloudWriterCommand.h"

#include <gnomonCore/gnomonAlgorithm/gnomonPointCloud/gnomonAbstractPointCloudWriter.h>
#include <gnomonCore/gnomonPythonPluginLoader.h>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class gnomonPointCloudWriterCommandPrivate
{
public:
    std::shared_ptr<gnomonPointCloudSeries> pointCloud = nullptr;
};

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

gnomonPointCloudWriterCommand::gnomonPointCloudWriterCommand() : d(new gnomonPointCloudWriterCommandPrivate)
{
    GNOMON_COMMANDS_INIT(gnomonPointCloudWriterCommand)

        //see MeshWriterCommand for what to do when there is multiple writers
    QStringList keys = gnomonCore::pointCloudWriter::pluginFactory().keys();
    if (!keys.empty()) {
        this->algorithm_name = keys[0];
        this->action = gnomonCore::pointCloudWriter::pluginFactory().create(this->algorithm_name);
    }
}

gnomonPointCloudWriterCommand::~gnomonPointCloudWriterCommand()
{
    delete d;
}

void gnomonPointCloudWriterCommand::setAlgorithmName(const QString& algo_name)
{
    this->algorithm_name = algo_name;

        delete this->action;
    this->action = gnomonCore::pointCloudWriter::pluginFactory().create(algo_name);
}


void gnomonPointCloudWriterCommand::predo(void)
{
    ((gnomonAbstractPointCloudWriter *) this->action)->setPath(this->m_path);
    if (d->pointCloud) {
        d->pointCloud->load();
    }
    ((gnomonAbstractPointCloudWriter *) this->action)->setPointCloud(d->pointCloud);
}

void gnomonPointCloudWriterCommand::postdo(void)
{

}

void gnomonPointCloudWriterCommand::undo()
{
    ((gnomonAbstractPointCloudWriter *) this->action)->setPath("");
}

void gnomonPointCloudWriterCommand::setPointCloud(std::shared_ptr<gnomonPointCloudSeries> pointCloud)
{
    d->pointCloud = pointCloud;
}

void gnomonPointCloudWriterCommand::setForm(std::shared_ptr<gnomonAbstractDynamicForm> form)
{
    d->pointCloud = std::dynamic_pointer_cast<gnomonPointCloudSeries>(form);
}

QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > gnomonPointCloudWriterCommand::inputs()
{
    QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > inputs;
    inputs["pointCloud"] = d->pointCloud;
    return inputs;
}

gnomonAbstractCommand::orderedMap gnomonPointCloudWriterCommand::inputTypes() {
    orderedMap input_types;
    input_types.emplace_back(std::make_pair("pointCloud", "gnomonPointCloud"));
    return input_types;
}

void gnomonPointCloudWriterCommand::setInputForm(const QString &name, std::shared_ptr<gnomonAbstractDynamicForm> form) {
    if (name == "pointCloud") {
        this->setPointCloud(std::dynamic_pointer_cast<gnomonPointCloudSeries>(form));
    } else {
        dtkWarn()<<Q_FUNC_INFO<<"Unknown input "<< name;
    }
}

void gnomonPointCloudWriterCommand::clear(void) {
    gnomonAbstractAlgorithmCommand::clear();
    d->pointCloud = nullptr;
}

GNOMON_REGISTER_TYPE(gnomonPointCloudWriterCommand)

//
// gnomonPointCloudWriterCommand.cpp ends here
