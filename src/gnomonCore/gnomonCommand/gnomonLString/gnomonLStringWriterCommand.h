#pragma once

#include <gnomonCore/gnomonCommand/gnomonAbstractWriterCommand>

#include <gnomonCore/gnomonForm/gnomonLString/gnomonLString>

class GNOMONCORE_EXPORT gnomonLStringWriterCommand : public gnomonAbstractWriterCommand
{
public:
     gnomonLStringWriterCommand(void);
    ~gnomonLStringWriterCommand(void) override;

public:
    void  predo(void) override;
    void postdo(void) override;
    void   undo(void) override;

public:
    void setForm(std::shared_ptr<gnomonAbstractDynamicForm> form) override;
    void setLString(std::shared_ptr<gnomonLStringSeries> image_series);
    void setAlgorithmName(const QString& algo_name) override;

    QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > inputs() override;

    void clear(void) override;

public:
    orderedMap inputTypes() override;

    void setInputForm(const QString &name, std::shared_ptr<gnomonAbstractDynamicForm> form) override;

public:
    inline static const QString groupName = "lStringWriter";

private:
    class gnomonLStringWriterCommandPrivate *d;
};

GNOMON_COMMAND_TRAITS(gnomonLStringWriterCommand)