#pragma once

#include <gnomonCoreExport>

#include "gnomonAbstractAlgorithmCommand.h"

#include <QtCore>

class gnomonAbstractDynamicForm;

class GNOMONCORE_EXPORT gnomonAbstractReaderCommand : public gnomonAbstractAlgorithmCommand
{
public:
    ~gnomonAbstractReaderCommand() override;

public:
    QMap<QString, QString> descriptions() const;
    QMap<QString, QStringList>  extensions() const;
    QStringList algorithmNames() const;
    QMap<QString, QString> preview() const;

public:
    void setAlgorithmName(const QString& algo_name) override;

public:
    const QString& path();
    void setPath(const QString& path);
    const QString& source();
    void setSource(const QString& source);

    QMap<QString, std::shared_ptr<gnomonAbstractDynamicForm> > inputs() override;

    orderedMap inputTypes() override;

    void setInputForm(const QString &name, std::shared_ptr<gnomonAbstractDynamicForm> form) override;

protected:
    QString m_path = "";
    QString m_source = "";
    QMap<QString, QString> m_preview;
    QMap<QString, QString> m_descriptions;
    QMap<QString, QStringList> m_extensions;
    QMap<QString, gnomonAbstractAlgorithm*> m_actions;
};

//
// gnomonAbstractReaderCommand.h ends here
