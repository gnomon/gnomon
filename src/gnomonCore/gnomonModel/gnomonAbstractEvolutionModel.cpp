#include "gnomonAbstractEvolutionModel.h"

#include "gnomonAbstractModel.h"
#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractEvolutionModel, evolutionModel, gnomonCore);
}

//
// gnomonAbstractEvolution.cpp ends here
