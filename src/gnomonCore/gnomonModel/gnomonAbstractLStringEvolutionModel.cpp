#include "gnomonAbstractLStringEvolutionModel.h"

#include "gnomonAbstractModel.h"
#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringEvolutionModel, lStringEvolutionModel, gnomonCore);
}

//
// gnomonAbstractLStringEvolutionModel.cpp ends here
