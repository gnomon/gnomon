#include "gnomonAbstractSystemScenario.h"

#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractSystemScenario, abstractSystemScenario, gnomonCore);
}

//
// gnomonAbstractSystemScenario.cpp ends here
