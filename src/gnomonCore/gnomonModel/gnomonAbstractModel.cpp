#include "gnomonAbstractModel.h"

#include "gnomonCore.h"

namespace gnomonCore {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractModel, abstractModel, gnomonCore);
}

//
// gnomonAbstractModel.cpp ends here
