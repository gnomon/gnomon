// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "gnomonPipelineEdge.h"

#include "gnomonPipelineNode.h"
#include "gnomonPipelinePort.h"

class gnomonPipelineEdgePrivate
{
public:
    gnomonPipelineNode *parent = nullptr;

public:
    gnomonPipelinePort *source = nullptr;
    gnomonPipelinePort *target = nullptr;

public:
    int formIndex = -1;
};

gnomonPipelineEdge::gnomonPipelineEdge(void) : d(new gnomonPipelineEdgePrivate)
{
}

gnomonPipelineEdge::~gnomonPipelineEdge(void)
{
    delete d;

    d = nullptr;
}


gnomonPipelinePort *gnomonPipelineEdge::source(void)
{
    return d->source;
}

gnomonPipelinePort *gnomonPipelineEdge::target(void)
{
    return d->target;
}

void gnomonPipelineEdge::setSource(gnomonPipelinePort *port)
{
    if (port != d->source) {
        d->source = port;
        emit sourceChanged();
    }
}

void gnomonPipelineEdge::setTarget(gnomonPipelinePort *port)
{
    if (port != d->target) {
        d->target = port;
        emit targetChanged();
    }
}

bool gnomonPipelineEdge::link(void)
{
    if (!d->source || !d->target)
        return false;

    if (d->source == d->target)
        return false;

    if (d->source->node() == d->target->node())
        return false;

    if (d->source->type() == gnomonPipelinePort::Output)
        d->source->node()->addOutputEdge(this);

    if (d->target->type() == gnomonPipelinePort::Input)
        d->target->node()->addInputEdge(this);

    return true;
}

bool gnomonPipelineEdge::unlink(void)
{
    if (!d->source || !d->target)
        return false;

    if (d->source->type() == gnomonPipelinePort::Output)
        d->source->node()->removeOutputEdge(this);

    if (d->target->type() == gnomonPipelinePort::Input)
        d->target->node()->removeInputEdge(this);

    return true;
}

gnomonPipelineNode *gnomonPipelineEdge::parent(void)
{
    return d->parent;
}

void gnomonPipelineEdge::setParent(gnomonPipelineNode *parent)
{
    if (parent != d->parent) {
        d->parent = parent;
        emit parentChanged();
    }
}

int gnomonPipelineEdge::formIndex(void)
{
    return d->formIndex;
}

void gnomonPipelineEdge::setFormIndex(int index)
{
    if (index != d->formIndex) {
        d->formIndex = index;
        emit formIndexChanged(index);
    }
    if (d->source) {
        d->source->setFormIndex(index);
    }
    if (d->target) {
        d->target->setFormIndex(index);
    }
}


//
// gnomonPipelineEdge.cpp ends here