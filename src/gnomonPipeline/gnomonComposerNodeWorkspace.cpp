// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "gnomonComposerNodeWorkspace.h"

gnomonComposerNodeWorkspace::gnomonComposerNodeWorkspace(const QString& title) : dtkComposerSceneNodeComposite()
{
    this->setTitle(title);
}

gnomonComposerNodeWorkspace::~gnomonComposerNodeWorkspace(void)
{

}

//
// gnomonComposerNodeWorkspace.cpp ends here
