// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <gnomonPipelineExport.h>

#include "gnomonPipelineNode.h"

class gnomonPipelinePort;

class GNOMONPIPELINE_EXPORT gnomonPipelineNodeReader : public gnomonPipelineNode
{
public:
     gnomonPipelineNodeReader(const QString &algorithm_class, const QString &algorithm,
                              const QString &path, QList<QString> outputs,
                              QJsonObject metadata={});
    ~gnomonPipelineNodeReader(void);

public:
    virtual QString path(void) override;
    virtual void setPath(const QString& path) override;

public:
    virtual QString toToml(void) override;
    virtual QString toLuigiClass(void) override;
    virtual const QJsonObject toJson(void) override;

protected:
    class gnomonPipelineNodeReaderPrivate *dd;
};

//
// gnomonPipelineNodeReader.h ends here