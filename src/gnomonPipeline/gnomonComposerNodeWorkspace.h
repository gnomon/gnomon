// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <gnomonPipelineExport.h>

#include <dtkComposer>

class GNOMONPIPELINE_EXPORT gnomonComposerNodeWorkspace : public dtkComposerSceneNodeComposite
{
public:
     gnomonComposerNodeWorkspace(const QString& title);
    ~gnomonComposerNodeWorkspace(void);
};

//
// gnomonComposerNodeWorkspace.h ends here
