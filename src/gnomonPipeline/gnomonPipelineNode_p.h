#pragma once

#include <QtCore>


class gnomonPipelineNodePrivate
{
public:
    QString name;
    QString description;
    QString version;
    QString algorithm_class;
    QString algorithm;

public:
    gnomonPipelineNode::Type type = gnomonPipelineNode::NODE_DEFAULT;

public:
    QPointF position;

/*public:
    QPointF offset;

public:
    QRectF rect;*/

public:
    QMap<QString, gnomonPipelinePort *> input_ports;
    QMap<QString, gnomonPipelinePort *> output_ports;

    QList<gnomonPipelineEdge *>  input_edges;
    QMap<QString, gnomonPipelineEdge *> input_edges_map;
    QList<gnomonPipelineEdge *> output_edges;

public:
    QString variantParameterString(const QVariant& parameter);
};



//
// gnomonPipelineNodePrivate.h ends here
