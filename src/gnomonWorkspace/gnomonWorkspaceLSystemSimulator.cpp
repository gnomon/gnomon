// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "gnomonWorkspaceLSystemSimulator.h"

#include <gnomonCore>
#include <gnomonVisualization>
#include <gnomonWidgets>
#include <gnomonMenuBar.h>

#include <dtkFonts>
#include <dtkThemes>
#include <dtkScript>
#include <dtkMacs>
#include <dtkWidgets>
#include <dtkWidgetsMenu+ux.h>

#include "gnomonVisualizations/gnomonLString/gnomonAbstractLStringMplVisualization.h"

// /////////////////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
//  gnomonHighlighterLString
// /////////////////////////////////////////////////////////////////////////////

class gnomonHighlighterLString : public QSyntaxHighlighter
{

public:
     gnomonHighlighterLString(QTextDocument *parent = nullptr);
    ~gnomonHighlighterLString(void);

public:
    void setModuleNames(const QStringList& module_names);

protected:
    void highlightBlock(const QString& text) override;

protected:
    QStringList module_names;
};

gnomonHighlighterLString::gnomonHighlighterLString(QTextDocument *parent) : QSyntaxHighlighter(parent)
{
}

gnomonHighlighterLString::~gnomonHighlighterLString(void)
{
}

void gnomonHighlighterLString::setModuleNames(const QStringList& module_names)
{
    this->module_names.clear();
    for (const auto& module_name : module_names) {
        this->module_names.append(module_name);
    }
}

void gnomonHighlighterLString::highlightBlock(const QString& text)
{
    QStringList theme_colors;
    theme_colors << "@red"  << "@green" << "@violet" <<  "@yellow"  << "@darkblue" << "@magenta";
    theme_colors << "@cyan" << "@teal"<< "@orange" << "@darkcyan" << "@blue";

    int i_module = 0;
    for (const auto& module_name : this->module_names) {
        QTextCharFormat module_format;
        module_format.setFontPointSize(24);
        module_format.setFontWeight(QFont::Bold);
        module_format.setForeground(dtkThemesEngine::instance()->color(theme_colors[i_module % theme_colors.size()]));
        i_module ++;

        QRegularExpression module_pattern(module_name);
        QRegularExpressionMatchIterator matchIterator = module_pattern.globalMatch(text);
        while (matchIterator.hasNext())
        {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(),match.capturedLength(),module_format);
        }
    }
}

// /////////////////////////////////////////////////////////////////////////////
//  gnomonPushButtonLPyAction
// /////////////////////////////////////////////////////////////////////////////

class gnomonPushButtonLPyAction : public QPushButton
{
public:
     gnomonPushButtonLPyAction(const QString&, const QString&, QWidget *parent = nullptr);
    ~gnomonPushButtonLPyAction(void);

public slots:
    void setEnabled(bool);

protected:
    QString path_on;
    QString path_off;
};

gnomonPushButtonLPyAction::gnomonPushButtonLPyAction(const QString& path_on, const QString& path_off, QWidget *parent) : QPushButton(parent)
{
    this->path_on = path_on;
    this->path_off = path_off;
    this->setIcon(QIcon(QPixmap(path_on).scaled(QSize(32,32),Qt::KeepAspectRatio,Qt::SmoothTransformation)));
    this->setStyleSheet("background: none; border: none; color: @fg");
    this->setIconSize(QSize(32,32));
}

gnomonPushButtonLPyAction::~gnomonPushButtonLPyAction(void)
{
}

void gnomonPushButtonLPyAction::setEnabled(bool enabled)
{
    QPushButton::setEnabled(enabled);
    if (enabled){
        this->setIcon(QIcon(QPixmap(path_on).scaled(QSize(32,32),Qt::KeepAspectRatio,Qt::SmoothTransformation)));
    } else {
        this->setIcon(QIcon(QPixmap(path_off).scaled(QSize(32,32),Qt::KeepAspectRatio,Qt::SmoothTransformation)));
    }
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class gnomonWorkspaceLSystemSimulatorPrivate
{
public:
    gnomonSpinner *spinner;

public:
    QTabWidget *lhs;
    QTabWidget *rhs;
    QTabWidget *params;
    QSplitter *splitter_lhs;
    QSplitter *splitter_rhs;
    QWidget *rhs_area;

public:
    gnomonMplView *axiom = nullptr;
    gnomonMplView *target = nullptr;

public:
    QTextEdit *axiom_editor = nullptr;
    gnomonHighlighterLString *highlighter = nullptr;

    QStackedWidget *axiom_stack = nullptr;
    QWidget *axiom_widget = nullptr;

public:
    gnomonPushButtonLPyAction *run_button;
    gnomonPushButtonLPyAction *stop_button;
    gnomonPushButtonLPyAction *rewind_button;
    gnomonPushButtonLPyAction *animate_button;
    gnomonPushButtonLPyAction *step_button;

    QCheckBox *use_axiom;

public:
    QList<dtkWidgetsMenu *> menus;

public:
    QWidget *in_code = nullptr;
    QWidget *in_axiom = nullptr;
    QWidget *out_view = nullptr;
    QVBoxLayout *code_editor_layout = nullptr;

    int edition_font_size;
    const int default_edition_font_size = 10;

    gnomonMenuBar *in_code_bar = nullptr;
    dtkWidgetsMenuBar *in_axiom_bar = nullptr;
    gnomonMenuBar *out_view_bar = nullptr;

    QVector<gnomonMenuBar*> menu_bars;

public:
    const QString setting_id = "LSystemSplittersSizes";
    const QString setting_id_lhs = setting_id + "LHS";
    const QString setting_id_rhs = setting_id + "RHS";

public:
    void exportAxiom(void);
    void disableFloatingDockWidgets(QWidget *parent);
    void verifyConflictInShortcuts(QWidget *parent);
    void setSplittersSizes(int width);
    void updateButtonsEnabled(bool runnning);
    void applyZoom(void);
};

void gnomonWorkspaceLSystemSimulatorPrivate::exportAxiom(void)
{
    QString edited_axiom = this->axiom_editor->toPlainText();

    if (edited_axiom != "") {
        gnomonLString *lstring = new gnomonLString();

        gnomonLStringSeries *lstring_series = new gnomonLStringSeries();
        lstring_series->insert(0, lstring);

        gnomonAbstractLStringData *lstring_data = gnomonCore::lStringData::pluginFactory().create("gnomonLStringDataLPy");
        lstring_data->fromString(edited_axiom);
        lstring->setData(lstring_data);

        this->axiom->setForm("gnomonLString",lstring_series);
    } else {
        if (this->axiom->form("gnomonLString")) {
            this->axiom->removeForm("gnomonLString");
        }
    }
}

void gnomonWorkspaceLSystemSimulatorPrivate::updateButtonsEnabled(bool running)
{
    for (auto* button : { run_button, step_button, rewind_button, animate_button }) {
        button->setEnabled(running == false);
    }
    stop_button->setEnabled(running);
};

void gnomonWorkspaceLSystemSimulatorPrivate::applyZoom(void)
{
    int stat;
    QString font_statement = "";
    font_statement += "from PyQt5 import Qt\n";
    font_statement += "from openalea.lpy.gui.lpycodeeditor import LpyCodeEditor\n";
    font_statement += "for top in Qt.QApplication.topLevelWidgets():\n";
    font_statement += "  for editor in top.findChildren(LpyCodeEditor):\n";
    font_statement += "    editor.setEditionFontSize(" + QString::number(edition_font_size) + ")\n";
    dtkScriptInterpreterPython::instance()->interpret(font_statement, &stat);
}

gnomonWorkspaceLSystemSimulator::gnomonWorkspaceLSystemSimulator(QWidget *parent) : dtkWidgetsWorkspace(parent)
{
    d = new gnomonWorkspaceLSystemSimulatorPrivate;

    // d->spinner = new gnomonSpinner(this);
    // d->spinner->start();

/////////////////////////////////////////////////////////////////////////////

    d->axiom = new gnomonMplView(this);
    d->axiom->setAcceptForm("gnomonLString",true);
    d->axiom->setInputView(true);

    d->axiom_editor = new QTextEdit(this);
    QFont font = d->axiom_editor->font();
    font.setPointSize(18);
    font.setFamily("Courier New");
    d->axiom_editor->setFont(font);
    d->axiom_editor->setAcceptDrops(false);

    d->highlighter = new gnomonHighlighterLString(d->axiom_editor->document());

    d->axiom_stack = new QStackedWidget(this);
    d->axiom_stack->addWidget(d->axiom);
    d->axiom_stack->addWidget(d->axiom_editor);
    d->axiom_stack->setCurrentWidget(d->axiom);

    QToolButton *axiom_figure_button = new QToolButton(this);
    axiom_figure_button->setIcon(dtkFontAwesome::instance()->icon(fa::square));
    axiom_figure_button->setToolTip("2D Form Viewer");
    QToolButton *axiom_editor_button = new QToolButton(this);
    axiom_editor_button->setIcon(dtkFontAwesome::instance()->icon(fa::edit));
    axiom_editor_button->setToolTip("Text Editor");

    QPushButton *axiom_save_button = new QPushButton("  Save  ");
    axiom_save_button->setVisible(false);

    QHBoxLayout *axiom_button_layout = new QHBoxLayout;
    axiom_button_layout->addWidget(axiom_figure_button);
    axiom_button_layout->addWidget(axiom_editor_button);
    axiom_button_layout->addStretch();
    axiom_button_layout->addWidget(axiom_save_button);

    QVBoxLayout *axiom_layout = new QVBoxLayout;
    axiom_layout->setContentsMargins(0, 0, 0, 0);
    axiom_layout->setSpacing(0);
    axiom_layout->addLayout(axiom_button_layout);
    axiom_layout->addWidget(d->axiom_stack);

    d->axiom_widget = new QWidget(this);
    d->axiom_widget->setLayout(axiom_layout);

    connect(axiom_save_button, &QToolButton::clicked, [=] (void) -> void
    {
        d->exportAxiom();
    });

    connect(axiom_figure_button, &QToolButton::clicked, [=] (void) -> void
    {
        d->exportAxiom();
        d->axiom_stack->setCurrentWidget(d->axiom);
        axiom_save_button->setVisible(false);
    });

    connect(axiom_editor_button, &QToolButton::clicked, [=] (void) -> void
    {
        d->axiom_stack->setCurrentWidget(d->axiom_editor);
        axiom_save_button->setVisible(true);
    });

    connect(d->axiom, &gnomonMplView::formAdded, [=] (const QString& form_name)
    {
        gnomonAbstractDynamicForm *form = d->axiom->form(form_name);

        if (gnomonLStringSeries *lstring_series = dynamic_cast<gnomonLStringSeries *>(form)) {
            gnomonLString *lstring = lstring_series->current()->asLString();
            QString lstring_value = lstring->toString();

            QStringList module_names;
            for (const auto & module_id :  lstring->moduleIds()) {
                QString module_name = lstring->moduleName(module_id);
                if (! module_names.contains(module_name)) {
                    module_names.append(module_name);
                }
            }
            qDebug()<<module_names;
            d->highlighter->setModuleNames(module_names);

            d->axiom_editor->blockSignals(true);
            d->axiom_editor->setText(lstring_value);
            d->axiom_editor->blockSignals(false);
        }
    });

    connect(d->axiom, &gnomonMplView::formRemoved, [=] (const QString& form_name)
    {
        if (form_name == "gnomonLString") {
            QStringList module_names;
            d->highlighter->setModuleNames(module_names);

            d->axiom_editor->blockSignals(true);
            d->axiom_editor->setText("");
            d->axiom_editor->blockSignals(false);
        }
    });

/////////////////////////////////////////////////////////////////////////////

    d->target = new gnomonMplView(this);
    d->target->setAcceptForm("gnomonLString",true);
    d->target->setInputView(false);

    d->lhs = new QTabWidget(this);
    d->lhs->setTabPosition(QTabWidget::South);
    d->lhs->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    d->rhs = new QTabWidget(this);
    d->rhs->setTabPosition(QTabWidget::South);

    d->params = new QTabWidget(this);
    d->params->setTabPosition(QTabWidget::South);
    d->params->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *params_layout = new QVBoxLayout;
    params_layout->addWidget(d->params);
    params_layout->addStretch();

    d->run_button = new gnomonPushButtonLPyAction(":gnomon/gnomonButton-Run.png",":gnomon/gnomonButton-Run-off.png");
    d->animate_button = new gnomonPushButtonLPyAction(":gnomon/gnomonButton-Animate.png",":gnomon/gnomonButton-Animate-off.png");
    d->stop_button = new gnomonPushButtonLPyAction(":gnomon/gnomonButton-Stop.png",":gnomon/gnomonButton-Stop-off.png");
    d->rewind_button = new gnomonPushButtonLPyAction(":gnomon/gnomonButton-Rewind.png",":gnomon/gnomonButton-Rewind-off.png");
    d->step_button = new gnomonPushButtonLPyAction(":gnomon/gnomonButton-Step.png",":gnomon/gnomonButton-Step-off.png");
    d->updateButtonsEnabled(false);

    d->run_button->setToolTip("Run");
    d->animate_button->setToolTip("Animate");
    d->stop_button->setToolTip("Pause");
    d->rewind_button->setToolTip("Rewind");
    d->step_button->setToolTip("Step");

    QHBoxLayout *controls_layout = new QHBoxLayout;
    controls_layout->setContentsMargins(0, 0, 0, 0);
    controls_layout->addWidget(d->run_button);
    controls_layout->addWidget(d->animate_button);
    controls_layout->addWidget(d->stop_button);
    controls_layout->addWidget(d->rewind_button);
    controls_layout->addWidget(d->step_button);
    controls_layout->setAlignment(Qt::AlignHCenter);

    QWidget *controls = new QWidget(this);
    controls->setLayout(controls_layout);

    d->use_axiom = new QCheckBox("Use external axiom");
    d->use_axiom->setTristate(false);
    d->use_axiom->setChecked(true);

    QWidget *dashboard = new QFrame;
    QLayout *dashboard_layout = new QVBoxLayout;
    dashboard->setLayout(dashboard_layout);
    dashboard_layout->addWidget(new QLabel("Simulation Parameters"));

    auto addToDashboard = [&] (QWidget *widget, bool add_separator, QLabel *title=nullptr)
    {
        if (add_separator) {
            QFrame *separator = new QFrame();
            separator->setFrameShape(QFrame::HLine);
            separator->setFrameShadow(QFrame::Raised);//Sunken);
            dashboard_layout->addWidget(separator);
        }
        if (title != nullptr) {
            dashboard_layout->addWidget(title);
        }
        dashboard_layout->addWidget(widget);
    };
    addToDashboard(d->params, true, new QLabel("Parameters"));
    addToDashboard(controls, true, new QLabel("Controls"));
    addToDashboard(d->use_axiom, false);

    d->rhs_area = new QWidget(this);

    QVBoxLayout *rhs_area_layout = new QVBoxLayout(d->rhs_area);
    rhs_area_layout->setContentsMargins(0, 0, 0, 0);
    rhs_area_layout->setSpacing(0);
    rhs_area_layout->addWidget(d->rhs);

    d->splitter_lhs = new QSplitter(this);
    d->splitter_lhs->addWidget(d->lhs);

    d->splitter_rhs = new QSplitter(this);
    d->splitter_rhs->addWidget(d->rhs_area);
    d->splitter_rhs->addWidget(dashboard);
    d->splitter_lhs->addWidget(d->splitter_rhs);

    for (auto *splitter: { d->splitter_lhs, d->splitter_rhs }) {
        splitter->setHandleWidth(5);
        splitter->setChildrenCollapsible(false);
        connect(splitter, &QSplitter::splitterMoved, [=] ()
        {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "gnomon");
            const auto is_lhs = (splitter == d->splitter_lhs);
            settings.setValue(is_lhs ? d->setting_id_lhs : d->setting_id_rhs, splitter->saveState());
        });
    }

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    // layout->addWidget(d->spinner);
    layout->addWidget(d->splitter_lhs);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    QFile file(":lpy.py");
    file.open(QIODevice::ReadOnly);
    QString script = file.readAll();
    file.close();

    QTimer::singleShot(500, [=] (void) -> void
    {
        int stat;
        dtkScriptInterpreterPython::instance()->interpret(script, &stat);
    });

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    this->enter();

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    loadPluginGroup("lStringData");
}

gnomonWorkspaceLSystemSimulator::~gnomonWorkspaceLSystemSimulator(void)
{
    delete d;
}

void gnomonWorkspaceLSystemSimulator::enter(void)
{
    foreach(dtkWidgetsMenu *menu, d->menus)
        dtkApp->window()->menubar()->addMenu(menu);

    dtkApp->window()->menubar()->touch();
}

void gnomonWorkspaceLSystemSimulator::leave(void)
{
    foreach(dtkWidgetsMenu *menu, d->menus)
        dtkApp->window()->menubar()->removeMenu(menu);

    dtkApp->window()->menubar()->touch();
}

void gnomonWorkspaceLSystemSimulator::apply(void)
{

}

void gnomonWorkspaceLSystemSimulator::apply(QWidget *view)
{

    int stat;
    dtkScriptInterpreterPython::instance()->interpret("import gnomon.core", &stat);
    dtkScriptInterpreterPython::instance()->interpret("from gnomon.core import gnomonLStringSeries, gnomonLString", &stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring = gnomonLString()", &stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_series = gnomonLStringSeries()", &stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_series.insert(0, gnomon_lstring)", &stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_data = gnomon.core.lStringData_pluginFactory().create('gnomonLStringDataLPy')",&stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_data.set_lstring(lstring)",&stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_data.this.disown()",&stat);
    dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring.setData(gnomon_lstring_data)",&stat);

    dtkScriptInterpreterPython::instance()->interpret("import gnomon.visualization", &stat);
    dtkScriptInterpreterPython::instance()->interpret("manager = gnomon.visualization.gnomonFormManager.instance()", &stat);

    dtkScriptInterpreterPython::instance()->interpret("from openalea.lpy.gui.lpystudio import Viewer", &stat);
    dtkScriptInterpreterPython::instance()->interpret("import tempfile", &stat);

    dtkScriptInterpreterPython::instance()->interpret("dir = tempfile.TemporaryDirectory()", &stat);
    dtkScriptInterpreterPython::instance()->interpret("Viewer.saveSnapshot(dir.name+'/snapshot.png')", &stat);

    dtkScriptInterpreterPython::instance()->interpret("from imageio import imread", &stat);
    dtkScriptInterpreterPython::instance()->interpret("from PIL import Image", &stat);
    dtkScriptInterpreterPython::instance()->interpret("import numpy as np", &stat);

    dtkScriptInterpreterPython::instance()->interpret("viewer_img = imread(dir.name+'/snapshot.png')", &stat);
    dtkScriptInterpreterPython::instance()->interpret("viewer_img = np.array(Image.fromarray(viewer_img).resize((600,600)))", &stat);
    dtkScriptInterpreterPython::instance()->interpret("image = [[[rgb for rgb in c] for c in r] for r in viewer_img]", &stat);
    dtkScriptInterpreterPython::instance()->interpret("dir.cleanup()", &stat);

    QString red_string = QString::number(this->color.red());
    QString green_string = QString::number(this->color.green());
    QString blue_string = QString::number(this->color.blue());
    QString color_statement = "color = ["+red_string+","+green_string+","+blue_string+"]";
    dtkScriptInterpreterPython::instance()->interpret(color_statement, &stat);

    dtkScriptInterpreterPython::instance()->interpret("manager.addForm(gnomon_lstring_series, color, image)", &stat);

//    gnomonLString *lstring = new gnomonLString();
//
//    gnomonLStringSeries *lstring_series = new gnomonLStringSeries();
//    lstring_series->insert(0, lstring);
//
//    qDebug() << Q_FUNC_INFO << 3 << current_lstring << gnomonCore::lStringData::pluginFactory().keys();
//
//    gnomonAbstractLStringData *lstring_data = gnomonCore::lStringData::pluginFactory().create("gnomonLStringDataLPy");
//
//    qDebug() << Q_FUNC_INFO << lstring_data;
//
//    lstring_data->fromString(current_lstring);
//    lstring->setData(lstring_data);
//
//    qDebug() << Q_FUNC_INFO << 4 << current_lstring;
//
//    QImage image(128, 128, QImage::Format_ARGB32);
//    image.fill(Qt::black);
//    view->render(&image);
//
//    qDebug() << Q_FUNC_INFO << 5 << image;
//
//    GNOMON_FORM_MANAGER->addForm(lstring_series, this->color, image);
//
    qDebug() << Q_FUNC_INFO << "Done";
}

void gnomonWorkspaceLSystemSimulator::reparentAction(QMenuBar * menu, const char * menuLabel, const char * actionLabel, QPushButton * button)
{
    foreach(QAction *action, menu->actions()) {

        qDebug() << Q_FUNC_INFO << action->text();

        if(action->text() == menuLabel) {

            foreach(QAction *reaction, action->menu()->actions()) {

                if(reaction->text() == actionLabel) {

                    this->connect(button, &QPushButton::clicked, [=] (void) -> void
                    {
                        if (button == d->stop_button) {
                            reaction->trigger();
                            return;
                        }
                        d->updateButtonsEnabled(true);

                        int stat;

                        if (d->use_axiom->isChecked()) {
                            dtkScriptInterpreterPython::instance()->interpret("import gnomon.core", &stat);
                            dtkScriptInterpreterPython::instance()->interpret("from gnomon.core import gnomonLStringSeries, gnomonLString", &stat);
                            dtkScriptInterpreterPython::instance()->interpret("from gnomon.visualization import getFigureForm, addFormToFigure", &stat);

                            dtkScriptInterpreterPython::instance()->interpret("import openalea.lpy as lpy", &stat);
                            dtkScriptInterpreterPython::instance()->interpret("from openalea.lpy.gui.lpystudio import LPyWindow", &stat);
                            dtkScriptInterpreterPython::instance()->interpret("from openalea.lpy.gui.lpycodeeditor import LpyCodeEditor", &stat);
                            dtkScriptInterpreterPython::instance()->interpret("from PyQt5 import Qt", &stat);

                            QString get_statement = "";
                            get_statement += "form = getFigureForm('gnomonLString',";
                            get_statement += QString::number(d->axiom->figureNumber());
                            get_statement += ")";
                            dtkScriptInterpreterPython::instance()->interpret(get_statement, &stat);

                            QString axiom_statement = "";
                            axiom_statement += "if form is not None:\n";
                            axiom_statement += "  axiom_lstring = form.current().asLString()\n";
                            axiom_statement += "  gnomon_axiom = axiom_lstring.toString()\n";
                            axiom_statement += "else:\n";
                            axiom_statement += "  gnomon_axiom = None\n";
                            axiom_statement += "for top in Qt.QApplication.topLevelWidgets():\n";
                            axiom_statement += "  for editor in top.findChildren(LpyCodeEditor):\n";
                            axiom_statement += "    editor.setAxiom(gnomon_axiom)\n";
                            dtkScriptInterpreterPython::instance()->interpret(axiom_statement, &stat);
                        }

                        reaction->trigger();

                        qDebug()<<Q_FUNC_INFO<<"Run finished";

                        QString axiom_clear_statement = "";
                        axiom_clear_statement += "for top in Qt.QApplication.topLevelWidgets():\n";
                        axiom_clear_statement += "  for editor in top.findChildren(LpyCodeEditor):\n";
                        axiom_clear_statement += "    editor.setAxiom(None)\n";
                        dtkScriptInterpreterPython::instance()->interpret(axiom_clear_statement, &stat);

                        QString simu_statement = "";
                        simu_statement += "for top in Qt.QApplication.topLevelWidgets():\n";
                        simu_statement += "  for lpy_window in top.findChildren(LPyWindow):\n";
                        simu_statement += "    simu = lpy_window.currentSimulation()\n";
                        simu_statement += "    simu.updateLsystemCode()\n";
                        simu_statement += "    simu.isTextEdited()\n";
                        dtkScriptInterpreterPython::instance()->interpret(simu_statement, &stat);

                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_series = gnomonLStringSeries()", &stat);
                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring = gnomonLString()", &stat);
                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_series.insert(0, gnomon_lstring)", &stat);
                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_data = gnomon.core.lStringData_pluginFactory().create('gnomonLStringDataLPy')",&stat);
                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_data.set_lstring(lstring)",&stat);
                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring_data.this.disown()",&stat);
                        dtkScriptInterpreterPython::instance()->interpret("gnomon_lstring.setData(gnomon_lstring_data)",&stat);

                        QString add_statement = "";
                        add_statement += "addFormToFigure(gnomon_lstring_series,'gnomonLString',";
                        add_statement += QString::number(d->target->figureNumber());
                        add_statement += ")";
                        dtkScriptInterpreterPython::instance()->interpret(add_statement, &stat);

                        d->updateButtonsEnabled(false);
                    });
                }
            }
        }
    }
}

void gnomonWorkspaceLSystemSimulatorPrivate::disableFloatingDockWidgets(QWidget *parent)
{
    static QSet<QWidget *> already_disabled;
    if (already_disabled.contains(parent)) {
        return;
    }
    already_disabled << parent;

    auto *as_dock_widget = dynamic_cast<QDockWidget *>(parent);
    if (as_dock_widget != nullptr) {
        as_dock_widget->setFeatures(as_dock_widget->features() & ~(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable));
    }
    for (auto *child: parent->children()) {
        auto *as_widget = dynamic_cast<QWidget *>(child);
        if (as_widget != nullptr) {
            disableFloatingDockWidgets(as_widget);
        }
    }
}

void printParents(QWidget *root, int depth=0)
{
    if (root != nullptr) {
        QString prefix;
        for (int i=0; i<depth; ++i) { prefix += " "; }
        qDebug() << prefix << root;
        printParents(root->parentWidget(), depth + 1);
    }
}

void gnomonWorkspaceLSystemSimulatorPrivate::verifyConflictInShortcuts(QWidget *parent)
{
    static QMap<QString, QAction*> binds;
    static QSet<QAction *> actions;
    auto describe_action = [] (const QAction *action) {
        QString ret = action->text();
        ret.replace("&", "");
        const auto *parent = action->parentWidget();
        if (parent != nullptr && parent->objectName().size()) {
            ret += " (parented to " + parent->objectName() + ")";
        }
        return ret;
    };
    for (auto *widget: parent->findChildren<QWidget *>()) {
        for (auto *action: widget->actions()) {
            if (actions.contains(action)) {
                continue;
            }
            const auto& shortcut = action->shortcut();
            if (shortcut.isEmpty()) {
                continue;
            }
            actions << action;
            const auto& key_sequence = shortcut.toString();
            if (binds.contains(key_sequence)) {
                qDebug() << "Conflict in shortcuts:" << describe_action(action) << "and" << describe_action(binds[key_sequence]) << "are both bound to" << key_sequence;
            } else {
                binds[key_sequence] = action;
            }
        }
    }
}

void gnomonWorkspaceLSystemSimulator::fill(QWidget *widget)
{
    qDebug() << Q_FUNC_INFO << widget << widget->objectName();

    static QList<QWidget *> filled;

    if(filled.contains(widget))
        return;

    d->disableFloatingDockWidgets(widget);
    d->verifyConflictInShortcuts(widget);

// /////////////////////////////////////////////////////////////////////////////
// LPYCodeEditor
// /////////////////////////////////////////////////////////////////////////////

    if(widget->objectName() == "LPYCodeEditor") {

        d->in_code = new QWidget(this);
        auto *code_editor_frame = new QWidget(this);
        d->in_code_bar = new gnomonMenuBar(d->in_code, code_editor_frame);
        d->menu_bars << d->in_code_bar;

        d->code_editor_layout = new QVBoxLayout;
        code_editor_frame->setLayout(d->code_editor_layout);
        d->code_editor_layout->addWidget(widget);

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "gnomon");
        d->edition_font_size = settings.value("LPyEditionFontSize", d->default_edition_font_size).toInt();
        d->applyZoom();

        // if(QTextEdit *edit = dynamic_cast<QTextEdit *>(widget))
        //     edit->setFrameShape(QFrame::NoFrame);

        d->lhs->addTab(d->in_code, "Code");

        auto tab_bars = widget->findChildren<QTabBar*>("documentNames");
        Q_ASSERT_X(tab_bars.size() == 1, Q_FUNC_INFO, (QString("failed to find 1 instance of QTabBar documentNames, results size=") + QString::number(tab_bars.size())).toStdString().c_str());
        auto *tab_bar = tab_bars[0];
        tab_bar->setTabsClosable(true);
        tab_bar->setExpanding(true);

        connect(tab_bar, &QTabBar::tabCloseRequested, [] (int index) {
            int stat;
            QString statement = "";
            statement += "from PyQt5 import Qt\n";
            statement += "from openalea.lpy.gui.lpystudio import LPyWindow\n";
            statement += "for top in Qt.QApplication.topLevelWidgets():\n";
            statement += "  for lpy_window in top.findChildren(LPyWindow):\n";
            statement += "    lpy_window.closeDocument(" + QString::number(index) + ")\n";
            dtkScriptInterpreterPython::instance()->interpret(statement, &stat);
        });

    }

// /////////////////////////////////////////////////////////////////////////////
// LPYShell
// /////////////////////////////////////////////////////////////////////////////

    if(widget->objectName() == "LPYShell") {
        d->rhs->addTab(widget, "Shell");
    }

    else if(widget->objectName() == "LPYDebug") {
        d->rhs->addTab(static_cast<QDockWidget *>(widget)->widget(), "Debug");
    }

    else if(widget->objectName() == "LPYParameters") {
        d->params->addTab(static_cast<QDockWidget *>(widget)->widget(), "Parameters");
    }

    else if(widget->objectName() == "LPYScalars") {
        d->params->addTab(static_cast<QDockWidget *>(widget)->widget(), "Scalars");
    }

    else if(widget->objectName() == "LPYCurves") {
        d->params->addTab(static_cast<QDockWidget *>(widget)->widget(), "Graphical Objects");
    }

    else if(widget->objectName() == "LPYMaterials") {
        d->params->addTab(static_cast<QDockWidget *>(widget)->widget(), "Materials");
    }

    else if(widget->objectName() == "LPYMainWindow") {

        auto find_frame = [=] (const QString& name) {
            auto frames = widget->findChildren<QFrame*>(name);
            if (frames.size() == 1) {
                return frames[0];
            } else {
                return (QFrame*)nullptr;
            }
        };

        Q_ASSERT(d->code_editor_layout != nullptr);
        for (auto *frame : { find_frame("frameFind"), find_frame("LPYframeFind"), find_frame("frameReplace") }) {
            if (frame == nullptr) {
                continue;
            }
            d->code_editor_layout->addWidget(frame);
        }

        if(QMainWindow *window = dynamic_cast<QMainWindow *>(widget)) {

            window->setParent(this);

            foreach(QAction *action, window->menuBar()->actions()) {

                QWidget *code_editor = nullptr;
                for (auto *widget: QApplication::topLevelWidgets()) {
                    for (auto *editor: widget->findChildren<QWidget *>("codeeditor")) {
                        code_editor = editor;
                        break;
                    }
                }
                Q_ASSERT(code_editor != nullptr);

                qDebug() << Q_FUNC_INFO << action->text();

                if(action->text() == "File") {
                    d->in_code_bar->addRootMenu(code_editor, action->menu());
                }

                if(action->text() == "Help") {
                    d->in_code_bar->addRootMenu(code_editor, action->menu());
               }

                if(action->text() == "Edit") {
                    d->in_code_bar->addRootMenu(code_editor, action->menu());
                }

                if(action->text() == "L-systems") {
                    auto *tools_menu = new QMenu("Tools", d->in_code);

                    foreach(QAction *reaction, action->menu()->actions()) {
                        if(reaction->text() == "Debug" || reaction->text() == "Profile") {
                            tools_menu->addAction(reaction);
                        }
                    }

                    d->in_code_bar->addRootMenu(code_editor, tools_menu);
                }

                if(action->text() == "View") {

                    QMenu *view_menu = new QMenu("View", d->in_code);

                    for (int zoom : { +1, -1, 0 } ){
                        QAction *action = new QAction;
                        QString label = "Zoom ";
                        label += (zoom > 0) ? "In" : (zoom < 0 ? "Out" : "1:1");
                        action->setText(label);
                        if (zoom > 0) {
                            action->setShortcut(QKeySequence::ZoomIn);
                        } else if (zoom < 0) {
                            action->setShortcut(QKeySequence::ZoomOut);
                        }
                        action->setShortcutContext(Qt::WindowShortcut);

                        view_menu->addAction(action);
                        connect(action, &QAction::triggered, [=] () {
                            const int zoom_step = 1;
                            if (zoom == 0) {
                                d->edition_font_size = d->default_edition_font_size;
                            } else {
                                const auto offset = zoom_step * zoom;
                                const auto new_font_size = d->edition_font_size + offset;
                                if (new_font_size > 0) {
                                    d->edition_font_size = new_font_size;
                                }
                            }

                            d->applyZoom();

                            QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "gnomon");
                            settings.setValue("LPyEditionFontSize", d->edition_font_size);

                        });
                    }
                    d->in_code_bar->addRootMenu(code_editor, view_menu);

                    foreach(QAction *reaction, action->menu()->actions()) {

                        if(reaction->text().contains("Tab")) {
                            reaction->trigger();
                        }
                    }
                }
            }

            this->reparentAction(window->menuBar(), "L-systems", "Run", d->run_button);
            this->reparentAction(window->menuBar(), "L-systems", "Step", d->step_button);
            this->reparentAction(window->menuBar(), "L-systems", "Rewind", d->rewind_button);
            this->reparentAction(window->menuBar(), "L-systems", "Animate", d->animate_button);
            this->reparentAction(window->menuBar(), "L-systems", "Stop", d->stop_button);

//            window->setMenuBar(0);

            this->enter();
        }
    }

    if(widget->objectName() == "PGLFrameGL") {

        d->out_view = new QWidget(this);
        auto *out_view_frame = new QWidget(this);
        d->out_view_bar = new gnomonMenuBar(d->out_view, out_view_frame);
        d->menu_bars << d->out_view_bar;

        auto *out_view_layout = new QVBoxLayout;
        out_view_frame->setLayout(out_view_layout);
        out_view_layout->addWidget(widget->parentWidget());

        d->rhs->addTab(d->out_view, "3D");

        d->rhs->addTab(d->target, "LString");

        if(QMainWindow *window = dynamic_cast<QMainWindow *>(widget->parentWidget())) {

            window->statusBar()->setSizeGripEnabled(false);
            foreach(QWidget *widget, window->findChildren<QToolBar *>()) {

                if(widget->objectName() == "LocationBar")
                    continue;

                if(widget->objectName() == "LineWidthBar")
                    continue;

                if(widget->objectName() == "TransitionBar")
                    continue;

                widget->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
                widget->setVisible(true); // NOTE: Does the trick! Com'on ....

                // dynamic_cast<QHBoxLayout *>(d->rhs_area->layout())->insertWidget(0, widget);

                qDebug() << "Got a toolbar!" << widget;
            }

            foreach(QAction *action, window->menuBar()->actions()) {
                if (action->text().size() == 0 || action->menu() == nullptr) {
                    continue;
                }
                d->out_view_bar->addRootMenu(widget, action->menu());
            }

            d->out_view_bar->touch();
        }

        d->setSplittersSizes(this->width());
    }

    if(widget->objectName() == "LPYViewer") {

        // QWidget *window = widget->window();

        d->rhs->addTab(widget, "3D");

        // window->hide();
    }

    if(widget->objectName() == "LPYAxiomViewer") {

        // QWidget *window = widget->window();

//        d->in_axiom = new QWidget;
//
//        d->in_axiom_bar = new dtkWidgetsMenuBar(d->in_axiom);
//        d->in_axiom_bar->show();
//        d->in_axiom_bar->setInteractive(false);
//        d->in_axiom_bar->setWidth(32);
//        d->in_axiom_bar->setMargins(6);
//        // d->view_menubar->addMenu(d->menu());
//        d->in_axiom_bar->touch();
//
//        QHBoxLayout *layout = new QHBoxLayout;
//        layout->setContentsMargins(0, 0, 0, 0);
//        layout->setSpacing(0);
//        layout->addWidget(d->in_axiom_bar);
//        layout->addWidget(d->in_axiom_bar->container());
//        layout->addWidget(widget);
//
//        d->in_axiom->setLayout(layout);

//        d->lhs->addTab(d->in_axiom, "Axiom");
        d->lhs->addTab(d->axiom_widget, "Axiom");

        // d->in_axiom_bar->setFixedHeight(d->in_axiom->height() + 150);

        // window->hide();
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    // d->spinner->stop();
    // d->spinner->hide();
    // d->splitter->show();

    // widget->setParent(d->splitter);


    // d->splitter->addWidget(widget);

// /////////////////////////////////////////////////////////////////////////////

    // widget->show();

    filled << widget;
}

void gnomonWorkspaceLSystemSimulatorPrivate::setSplittersSizes(int width)
{
    const auto width_panel1 = width * 33 / 100;
    const auto width_panels23 = width - width_panel1;
    const auto width_panel3 = width * 25 / 100;
    const auto width_panel2 = width_panels23 - width_panel3;

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "gnomon");
    if (settings.contains(setting_id_lhs)) {
        splitter_lhs->restoreState(settings.value(setting_id_lhs).toByteArray());
    } else {
        splitter_lhs->setSizes( { width_panel1, width_panels23 } );
    }

    if (settings.contains(setting_id_rhs)) {
        splitter_rhs->restoreState(settings.value(setting_id_rhs).toByteArray());
    } else {
        splitter_rhs->setSizes( { width_panel2, width_panel3 } );
    }
}

const QColor gnomonWorkspaceLSystemSimulator::color = QColor("#89a348");

bool gnomonWorkspaceLSystemSimulator::isEmpty(void)
{
    loadPluginGroup("lStringFrameMplVisualization");
    if (gnomonVisualization::lStringFrameMplVisualization::pluginFactory().keys().count() == 0) {
        return true;
    }
    int stat;
    dtkScriptInterpreterPython::instance()->interpret("import openalea.lpy", &stat);
    return (stat == 1) && (gnomonCore::lStringData::pluginFactory().keys().contains("gnomonLStringDataLPy"));
}

void gnomonWorkspaceLSystemSimulator::resizeEvent(QResizeEvent *event)
{
    d->params->setFixedHeight(event->size().height() - 225);

    for (auto *menu_bar: d->menu_bars) {
        menu_bar->resizeEvent();
    }

    // if (d->in_axiom && d->in_axiom_bar)
    //     d->in_axiom_bar->setFixedHeight(d->in_axiom->height());

    dtkWidgetsWorkspace::resizeEvent(event);
}

//
// gnomonWorkspaceLSystemSimulator.cpp ends here
