#include "gnomonAlgorithmWorkspace.h"
#include "gnomonAlgorithmWorkspace_p.h"
#include "gnomonProject"

#include <gnomonCore/gnomonCommand/gnomonAbstractAlgorithmCommand.h>

#include <gnomonPipeline/gnomonPipelineManager.h>

#include <gnomonVisualization/gnomonManager/gnomonFormManager.h>
#include <gnomonVisualization/gnomonView/gnomonVtkViewPool.h>

// /////////////////////////////////////////////////////////////////////////////
// gnomonAlgorithmWorkspacePrivate
// /////////////////////////////////////////////////////////////////////////////


gnomonAlgorithmWorkspacePrivate::gnomonAlgorithmWorkspacePrivate(void)
{
    this->pipeline_manager = gnomonPipelineManager::instance();
}

gnomonAlgorithmWorkspacePrivate::~gnomonAlgorithmWorkspacePrivate(void)
{
    if(this->pool)
        delete pool;

    if(this->sources)
        delete this->sources;

    if(this->targets)
        delete this->targets;

    if (this->figure)
        delete this->figure;

    if (this->text_view)
        delete this->text_view;
}

bool gnomonAlgorithmWorkspacePrivate::setAlgorithm(const QString& algorithm)
{
    if (algorithm != this->algorithm) {
        this->command->setAlgorithmName(algorithm);
        this->algorithm = algorithm;
        return true;
    }
    return false;
}

void gnomonAlgorithmWorkspacePrivate::registerPipeline(void)
{
    if (this->command) {
        this->pipeline_manager->addAlgorithm(this->command);
    }
}

void gnomonAlgorithmWorkspacePrivate::updatePool(void)
{
    if(!this->pool)
        this->pool = new gnomonVtkViewPool(this);

    foreach(gnomonVtkView *view, this->sources->views())
        this->pool->addView(view);

    foreach(gnomonVtkView *view, this->targets->views())
        this->pool->addView(view);
}

// /////////////////////////////////////////////////////////////////////////////
// gnomonAlgorithmWorkspace
// /////////////////////////////////////////////////////////////////////////////

gnomonAlgorithmWorkspace::gnomonAlgorithmWorkspace(QObject *parent) : gnomonAbstractWorkspace(parent)
{
    d = new gnomonAlgorithmWorkspacePrivate;

    d->sources = new gnomonVtkViewList(this);
    connect(d->sources, &gnomonVtkViewList::viewAdded, [=] (gnomonVtkView *v) {
        v->setInputView(true);
    });

    d->targets = new gnomonVtkViewList(this);
    connect(d->targets, &gnomonVtkViewList::viewAdded, [=] (gnomonVtkView *v) {
        auto connect_export = connect(v, &gnomonVtkView::exportedForm, [=] (std::shared_ptr<gnomonAbstractDynamicForm> f) {
            d->registerPipeline();
            d->pipeline_manager->addForm(f->uuid());
            this->m_can_be_destroyed = false;
            emit canBeDestroyedChanged(false);
        });
        d->connect_target_view_exports.push_back(connect_export);
    });

    connect(d->sources, &gnomonVtkViewList::formsChanged, [=] ()
    {
        this->setInputs();
        emit parametersChanged();
    });

    connect(this, &gnomonAlgorithmWorkspace::parametersChanged, [=] () {
        emit stateChanged();
    });

    d->timer.setInterval(100);
    connect(&d->timer, &QTimer::timeout, [=]() {
        //qDebug() << "============= PROGRESS : " << d->command->progress();
        emit progressChanged(d->command->progress());
        emit progressMessageChanged(d->command->progressMessage());
    });
    connect(this, &gnomonAbstractWorkspace::started, [=]() {
       d->timer.start();
    });
    connect(this, &gnomonAbstractWorkspace::finished, [=]() {
        d->timer.stop();
    });
}

gnomonAlgorithmWorkspace::~gnomonAlgorithmWorkspace(void)
{
    delete d;
}

QString gnomonAlgorithmWorkspace::algoName(void) const
{
    return d->algorithm;
}

QJsonObject gnomonAlgorithmWorkspace::algoMetaData(void) const
{
    QJsonObject algo_json;
    algo_json.insert("name", d->command->algorithmName());
    algo_json.insert("documentation", d->command->documentation());
    algo_json.insert("version", d->command->version());
    algo_json.insert("group", d->command->factoryName());
    return algo_json;
}

QStringList gnomonAlgorithmWorkspace::algorithms(void) const
{
    return d->keys;
}

QVariantList gnomonAlgorithmWorkspace::algorithmsData(void) const
{
    return d->algorithmsData;
}

void gnomonAlgorithmWorkspace::setAlgoName(const QString& algorithm)
{
    if (d->setAlgorithm(algorithm)) {
        emit algorithmChanged(algorithm);
        d->command->undo();
        this->setInputs();
        for(auto & param:d->command->parameters()) {
            param->connect([=] {
                emit stateChanged();
            });
        }
        emit parametersChanged();
    }
}

int gnomonAlgorithmWorkspace::currentIndex(void) const {
    return d->currentIndex;
}

void gnomonAlgorithmWorkspace::setCurrentIndex(int i) {
    if ((d->currentIndex != i) & (i < d->keys.size())) {
        d->currentIndex = i;
        emit currentIndexChanged();
        this->setAlgoName(d->keys[i]);
    }
}

void gnomonAlgorithmWorkspace::export_outputs(void) {
    for(const auto &output_view: d->targets->views()) {
        output_view->transmit();
    }
}

QJSValue gnomonAlgorithmWorkspace::parameters(void)
{
    QJSValue parameters = dtkCoreParameterCollection(d->command->parameters()).toJSValue(this);
    QMap<QString, QString> parameter_groups = d->command->parameterGroups();

    QJSValueIterator it(parameters);
    while (it.hasNext()) {
        it.next();
        QString group = parameter_groups.contains(it.name()) ? parameter_groups[it.name()] : "";
        it.value().setProperty("group", group != "" ? group : nullptr);
    }
    return parameters;
}

QStringList gnomonAlgorithmWorkspace::parameterNames(void)
{
    return d->command->parameters().keys();
}

void gnomonAlgorithmWorkspace::setParameter(const QString& parameter, const QVariant& value)
{
    d->command->setParameter(parameter, value);
    emit parametersChanged();
}

gnomonVtkViewList* gnomonAlgorithmWorkspace::sources(void) const
{
    return d->sources;
}

gnomonVtkViewList* gnomonAlgorithmWorkspace::targets(void) const
{
    return d->targets;
}

gnomonMplView* gnomonAlgorithmWorkspace::figure(void) const
{
    return d->figure;
}

gnomonQmlView* gnomonAlgorithmWorkspace::textView(void) const
{
    return d->text_view;
}

void gnomonAlgorithmWorkspace::run(bool no_async)
{
    Q_ASSERT(d->command);

    disconnect(d->connect_finished);
    if(!no_async) {
        d->connect_finished = connect(d->command, &gnomonAbstractCommand::finished, [this]() {
            this->viewOutputs();
            emit finished();
        });
    }

    emit started();

    // this->setInputs();

    if(no_async){
        d->command->setNoAsync();
    }

    d->command->redo();

    if(no_async){
        this->viewOutputs();
    }
}

void gnomonAlgorithmWorkspace::setInputs()
{
    // you need to overwrite this function if you don't have an exact mapping between
    // the number of views (sources) and the number of input types for your command,
    // and that all inputs can not be loaded from a single view.

    if (d->sources->views().size() == 1) {
        for(auto [name, input_type] : d->command->inputTypes()) {
            d->command->setInputForm(name, (*d->sources)[0]->form(input_type));
        }
    } else  if (d->command->inputs().size() == d->sources->views().size()) {
        int i=0;
        for(auto [name, input_type] : d->command->inputTypes()) {
            d->command->setInputForm(name, (*d->sources)[i]->form(input_type));
            ++i;
        }
    } else {
        dtkWarn() << Q_FUNC_INFO << "inputs size " <<d->command->inputs().size() << " but nb input views " << d->sources->views().size();
        return;
    }
}

void gnomonAlgorithmWorkspace::viewOutputs(void)
{
    Q_ASSERT(d->command);

    if((d->targets->views().size()>1) & (d->command->outputs().size() != d->targets->views().size())) {
        dtkWarn() << Q_FUNC_INFO << "outputs size " <<d->command->outputs().size() << " but nb output views " << d->targets->views().size();
        return;
    }

    bool empty_output = true;

    int i=0;

    for(auto [name, output_type] : d->command->outputTypes()) {
        if(d->command->outputs()[name] && (*d->targets)[i]->acceptedForms().contains(output_type)) {
            auto form =  d->command->outputs()[name];
            GNOMON_SESSION->trackForm(form);
            int form_count = GNOMON_FORM_MANAGER->formCount(output_type);
            form->metadata()->set("name", output_type.remove("gnomon") + QString::number(form_count+1));
            form->metadata()->set("source", d->algorithm);
            (*d->targets)[i]->setForm(output_type, form);
            (*d->targets)[i]->render();
            empty_output = false;
        }
        if(d->targets->views().size()>1)
            ++i;
    }

    if (!empty_output) {
        if(!this->target()->synced()) {
            this->target()->tryLinking();
        }
    }
}

QJsonObject gnomonAlgorithmWorkspace::_serialize(void) {
    QJsonObject state = gnomonAbstractWorkspace::_serialize();
    state.insert("algoName", algoName());
    state.insert("currentIndex", currentIndex());

    QVariantMap parameters_json;
    dtkCoreParameters dtkParameters = d->command->parameters();
    for(const auto& param_name : dtkParameters.keys()){
        auto param_value = dtkParameters[param_name]->toVariantHash();
        parameters_json.insert(param_name, QJsonObject::fromVariantHash(param_value));
    }
    state.insert("parameters", QJsonObject::fromVariantMap(parameters_json));

    QJsonArray sources;
    for (auto view : d->sources->views()) {
        sources.append(view->serialize());
    }
    QJsonArray targets;
    for (auto view : d->targets->views()) {
        targets.append(view->serialize());
    }
    state.insert("sources", sources);
    state.insert("targets", targets);
    if (d->figure) {
        state.insert("figure", d->figure->serialize());
    }
    if (d->text_view) {
        state.insert("text_view", d->text_view->serialize());
    }

    return state;
}

void gnomonAlgorithmWorkspace::_deserialize(const QJsonObject & state) {
    gnomonAbstractWorkspace::_deserialize(state);
    setCurrentIndex(state["currentIndex"].toInt());
    setAlgoName(state["algoName"].toString());

    QJsonObject parameters_json = state["parameters"].toObject();
    for(const auto& param_name: parameters_json.keys()) {
        auto param = parameters_json[param_name].toObject().toVariantHash();
        d->command->setParameter(param_name, dtkCoreParameter::create(param)->variant());
    }

    QJsonArray sources = state.value("sources").toArray();
    int i = 0;
    for (auto view : d->sources->views()) {
        view->deserialize(sources[i].toObject());
        i++;
    }
    QJsonArray targets = state.value("targets").toArray();
    i = 0;
    for (auto view : d->targets->views()) {
        view->deserialize(targets[i].toObject());
        i++;
    }
    if (state.contains("figure")) {
        d->figure->deserialize(state.value("figure").toObject());
    }
    if (state.contains("text_view")) {
        d->text_view->deserialize(state.value("text_view").toObject());
    }

    emit parametersChanged();
}


void gnomonAlgorithmWorkspace::restoreView(void) {
    for (auto view : d->sources->views()) {
        view->restoreState();
    }
    for (auto view : d->targets->views()) {
        view->restoreState();
    }
}

void gnomonAlgorithmWorkspace::hibernate(QString uuid) {
    gnomonAbstractWorkspace::hibernate(uuid);
    if(this->uuid() == uuid) {
        QList<std::shared_ptr<gnomonAbstractDynamicForm>> temp_holder; // prevent the forms from being outright deleted
        for (auto view : d->sources->views()) {
            for(auto form_name: view->formNames()) {
                temp_holder.append(view->form(form_name));
            }
        }
        for (auto view : d->targets->views()) {
            for(auto form_name: view->formNames()) {
                temp_holder.append(view->form(form_name));
            }
        }

        d->command->clear();
        for (auto view : d->sources->views()) {
            view->clear();
        }
        for (auto view : d->targets->views()) {
            view->clear();
        }

        for(const auto& form: temp_holder) {
            if(form.use_count() == 1) {
                // only the temp holder holds a reference
                deactivated_forms.append(form);
            }
        }
    }
}

void gnomonAlgorithmWorkspace::addInputView(const QVector<QString>& accepted_forms, QStringList nodePortNames) {
    if(nodePortNames.isEmpty()) {
        nodePortNames = d->command->inputs().keys();
    }

    if(accepted_forms.empty()) {
        QVector<QString> default_forms;
        for(auto [name, input_type] : d->command->inputTypes()) {
            default_forms.push_back(input_type);
        }
        d->sources->addView(default_forms, nodePortNames);
    } else {
        d->sources->addView(accepted_forms, nodePortNames);
    }
}

void gnomonAlgorithmWorkspace::addOutputView(const QVector<QString> &accepted_forms, QStringList nodePortNames) {
    if(nodePortNames.isEmpty()) {
        nodePortNames = d->command->outputs().keys();
    }

    if(accepted_forms.empty()) {
        QVector<QString> default_forms;
        for(auto [name, input_type] : d->command->outputTypes()) {
            default_forms.push_back(input_type);
        }
        d->targets->addView(default_forms, nodePortNames);
    } else {
        d->targets->addView(accepted_forms, nodePortNames);
    }
}

int gnomonAlgorithmWorkspace::progress(void) {
    return d->command->progress();
}

QString gnomonAlgorithmWorkspace::progressMessage(void) {
    return d->command->progressMessage();
}

void gnomonAlgorithmWorkspace::pause(void) {
    d->command->pause();
}

void gnomonAlgorithmWorkspace::resume(void) {
    d->command->resume();
}

void gnomonAlgorithmWorkspace::stop(void) {
    d->command->stop();
}

//
// gnomonAlgorithmWorkspace.cpp ends here
