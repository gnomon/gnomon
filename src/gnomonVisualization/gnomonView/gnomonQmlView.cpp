#include "gnomonQmlView.h"
#include "gnomonAbstractView_p.h"

#include <gnomonVisualization/gnomonManager/gnomonFormManager>

#include <gnomonCommand/gnomonAbstractQmlVisualizationCommand>
#include <gnomonCommand/gnomonDataDict/gnomonDataDictQmlVisualizationCommand>
#include <gnomonCommand/gnomonLString/gnomonLStringQmlVisualizationCommand>

QString transformMatrixString(QVector<QVector<double> > transform_matrix)
{
    QString matrix_string;

    matrix_string += "[";
    for (int row=0; row<transform_matrix.size(); row++) {
        if (row > 0) matrix_string += ",\n ";
        matrix_string += " [";
        for (int col=0; col<transform_matrix[row].size(); col++) {
            if (col > 0) matrix_string += ",";
            if (transform_matrix[row][col]>=0) matrix_string += " ";
            matrix_string += " " + QString::number(transform_matrix[row][col], 'f', 3);
        }
        matrix_string += "]";
    }
    matrix_string += " ]";

    return matrix_string;
}

QVector<QVector<double> > identity_matrix = { {1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1} };


// ///////////////////////////////////////////////////////////////////
// gnomonQmlViewPrivate
// ///////////////////////////////////////////////////////////////////

class gnomonQmlViewPrivate : public QObject
{
    Q_OBJECT

public:
    gnomonQmlViewPrivate(QObject *parent = Q_NULLPTR);
    ~gnomonQmlViewPrivate(void);

public:
    QString display_text;
    int font_size = 12;
};

gnomonQmlViewPrivate::gnomonQmlViewPrivate(QObject *parent): QObject(parent)
{
}

gnomonQmlViewPrivate::~gnomonQmlViewPrivate(void)
{
}

// ///////////////////////////////////////////////////////////////////
// gnomonQmlView
// ///////////////////////////////////////////////////////////////////

gnomonQmlView::gnomonQmlView(QObject *parent): gnomonAbstractView(parent)
{
    setObjectName("gnomonQmlView");
    dd = new gnomonQmlViewPrivate(this);
    d->q  = this;

    d->acceptForms["gnomonDataDict"] = false;
    d->acceptForms["gnomonLString"] = false;
}

void gnomonQmlView::setAcceptForm(const QString& form_type, bool accept) {
    if (!d->acceptForms.contains(form_type)) { // Form not supported by QmlView
        return;
    }
    d->acceptForms[form_type] = accept;

    if (!accept) {
        return;
    }

    if (form_type == "gnomonDataDict") {
        d->visualizationCommands["gnomonDataDict"] = std::make_shared<gnomonDataDictQmlVisualizationCommand>();
    } else if (form_type == "gnomonLString") {
        d->visualizationCommands["gnomonLString"] = std::make_shared<gnomonLStringQmlVisualizationCommand>();
    }

    d->visualizationCommands[form_type]->setView(this);
    connect(d->visualizationCommands[form_type].get(), &gnomonAbstractVisualizationCommand::visuParametersChanged, [=]() {
        emit formVisuParametersChanged();
    });
}

gnomonQmlView::~gnomonQmlView(void)
{
    delete dd;
}

const QString& gnomonQmlView::displayText(void)
{
    return dd->display_text;
}

void gnomonQmlView::setDisplayText(const QString& text)
{
    dd->display_text = text;
    emit displayTextChanged();
}

int gnomonQmlView::fontSize(void)
{
    return dd->font_size;
}

void gnomonQmlView::setFontSize(int size)
{
    if (size != dd->font_size && size >= 0) {
        dd->font_size = size;
        emit fontSizeChanged();
    }
}

void gnomonQmlView::render(void) {
    for(auto it = d->visualizationCommands.keyValueBegin(); it != d->visualizationCommands.keyValueEnd(); it++) {
        auto command = it->second;
        if(!command->inputs().contains(nullptr)) {
            command->update();
        }
    }
}

// ///////////////////////////////////////////////////////////////////

#include "gnomonQmlView.moc"

//
// gnomonQmlView.cpp ends here
