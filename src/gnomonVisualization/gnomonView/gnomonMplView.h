#pragma once

#include <gnomonVisualizationExport>

#include <QtCore>
#include <QtQml>

#include "gnomonAbstractView.h"

class gnomonAbstractDynamicForm;
class gnomonAbstractVisualization;
class gnomonAbstractMplVisualization;

class GNOMONVISUALIZATION_EXPORT gnomonMplView  : public gnomonAbstractView
{
    Q_OBJECT

public:
    gnomonMplView(QObject *parent = nullptr, bool no_python=false);
    virtual ~gnomonMplView(void);

public:
    Q_PROPERTY(int figureNumber READ figureNumber WRITE setFigureNumber NOTIFY figureNumberChanged);

    Q_PROPERTY(double xMin READ xMin WRITE setXMin NOTIFY limitsChanged);
    Q_PROPERTY(double xMax READ xMax WRITE setXMax  NOTIFY limitsChanged);
    Q_PROPERTY(double yMin READ yMin WRITE setYMin  NOTIFY limitsChanged);
    Q_PROPERTY(double yMax READ yMax WRITE setYMax  NOTIFY limitsChanged);

public:
    void setAcceptForm(const QString&, bool) override;
    void setAdaptedForm(const QString&, std::shared_ptr<gnomonAbstractDynamicForm>, std::shared_ptr<gnomonAbstractMplVisualization> visualization = nullptr);

public slots:
    void setFigureNumber(int num);

public:
    int figureNumber(void);

signals:
    void figureNumberChanged(int);

public slots:
    void setXMin(double x_min);
    void setXMax(double x_max);
    void setYMin(double y_min);
    void setYMax(double y_max);

    void updateLimits(void);

public:
    double xMin(void);
    double xMax(void);
    double yMin(void);
    double yMax(void);

signals:
    void limitsChanged(void);

signals:
    void figureCanvasReady(void);

public slots:
    void render(void) override;
    void clear(void) override;
    void saveScreenshot(const QString& filename) override;

public:
    QJsonObject serialize(void) override;
    void deserialize(const QJsonObject &serialization) override;

private:
    class gnomonMplViewPrivate *dd;
};

Q_DECLARE_METATYPE(gnomonMplView *);
//
// gnomonMplView.h ends here
