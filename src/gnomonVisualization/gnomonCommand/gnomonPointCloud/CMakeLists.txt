## #################################################################
## Sources
## #################################################################

ADD_GNOMON_SUBDIRECTORY_HEADERS(
        gnomonPointCloudVtkVisualizationCommand
        gnomonPointCloudVtkVisualizationCommand.h
)

ADD_GNOMON_SUBDIRECTORY_SOURCES(
        gnomonPointCloudVtkVisualizationCommand.cpp
)

######################################################################
### CMakeLists.txt ends here
