#include "gnomonVisualization.h"
#include "gnomonAbstractImageVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractImageVtkVisualization, imageVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractImageVtkVisualization.cpp ends here
