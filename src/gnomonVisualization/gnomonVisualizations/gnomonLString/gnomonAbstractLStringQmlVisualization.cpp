#include "gnomonVisualization.h"
#include "gnomonAbstractLStringQmlVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringQmlVisualization, lStringQmlVisualization, gnomonVisualization);
}

//
// gnomonAbstractLStringQmlVisualization.cpp ends here
