#include "gnomonVisualization.h"
#include "gnomonAbstractLStringMplVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringMplVisualization, lStringMplVisualization, gnomonVisualization);
}

//
// gnomonAbstractLStringMplVisualization.cpp ends here
