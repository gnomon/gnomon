#include "gnomonVisualization.h"
#include "gnomonAbstractLStringVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractLStringVtkVisualization, lStringVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractLStringVtkVisualization.cpp ends here
