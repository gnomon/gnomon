// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "gnomonVisualization.h"
#include "gnomonAbstractCellComplexVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellComplexVtkVisualization, cellComplexVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractCellComplexVtkVisualization.cpp ends here
