#include "gnomonVisualization.h"
#include "gnomonAbstractBinaryImageVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractBinaryImageVtkVisualization, binaryImageVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractBinaryImageVtkVisualization.cpp ends here
