#include "gnomonVisualization.h"
#include "gnomonAbstractPointCloudVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractPointCloudVtkVisualization, pointCloudVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractPointCloudVtkVisualization.cpp ends here
