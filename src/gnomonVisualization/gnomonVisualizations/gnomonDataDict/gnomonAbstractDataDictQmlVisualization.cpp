#include "gnomonVisualization.h"
#include "gnomonAbstractDataDictQmlVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractDataDictQmlVisualization, dataDictQmlVisualization, gnomonVisualization);
}

//
// gnomonAbstractDataDictQmlVisualization.cpp ends here
