#include "gnomonVisualization.h"
#include "gnomonAbstractMeshVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractMeshVtkVisualization, meshVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractMeshVtkVisualization.cpp ends here
