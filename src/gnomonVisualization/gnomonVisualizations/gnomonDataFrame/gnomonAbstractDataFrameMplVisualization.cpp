#include "gnomonVisualization.h"
#include "gnomonAbstractDataFrameMplVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractDataFrameMplVisualization, dataFrameMplVisualization, gnomonVisualization);
}

//
// gnomonAbstractDataFrameMplVisualization.cpp ends here
