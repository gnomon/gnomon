// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "gnomonVisualization.h"
#include "gnomonAbstractTreeMplVisualization.h"

#include <dtkCore>

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractTreeMplVisualization, treeMplVisualization, gnomonVisualization);
}

//
// gnomonAbstractTreeMplVisualization.cpp ends here
