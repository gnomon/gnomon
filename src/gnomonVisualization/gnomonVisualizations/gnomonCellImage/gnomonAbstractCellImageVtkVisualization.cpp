#include "gnomonVisualization.h"
#include "gnomonAbstractCellImageVtkVisualization.h"

namespace gnomonVisualization {
    GNOMON_DEFINE_CONCEPT(gnomonAbstractCellImageVtkVisualization, cellImageVtkVisualization, gnomonVisualization);
}

//
// gnomonAbstractCellImageVtkVisualization.cpp ends here
