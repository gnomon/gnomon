## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:


## #################################################################
## Sources
## #################################################################


ADD_GNOMON_SUBDIRECTORY_HEADERS(
    gnomonActor2DCellImage
    gnomonActor2DCellImage.h
    gnomonActorImage
    gnomonActorImage.h
    gnomonActorMesh
    gnomonActorMesh.h
    gnomonActorMesh_p.h
    gnomonActorMeshCellComplex
    gnomonActorMeshCellComplex.h
    gnomonActorMeshCellGraph
    gnomonActorMeshCellGraph.h
    gnomonActorMeshCellImage
    gnomonActorMeshCellImage.h
    gnomonActorScalarBar
    gnomonActorScalarBar.h
    gnomonActorVolume
    gnomonActorVolume.h)


ADD_GNOMON_SUBDIRECTORY_SOURCES(
    gnomonActor2DCellImage.cpp
    gnomonActorImage.cpp
    gnomonActorMesh.cpp
    gnomonActorMeshCellComplex.cpp
    gnomonActorMeshCellGraph.cpp
    gnomonActorMeshCellImage.cpp
    gnomonActorScalarBar.cpp
    gnomonActorVolume.cpp)


######################################################################
### CMakeLists.txt ends here
