import QtQuick      2.15

import gnomonQuick.Style as G

Rectangle {
    color: G.Style.colors.bgColor;

    implicitWidth: 1;
    implicitHeight: 1;
}
