import QtQuick            2.15
import QtQuick.Controls   2.15
import QtQuick.Layouts    1.15

// SwipeView {
StackLayout {

    id: _control;

    // interactive: false;

    Layout.fillWidth: true;
    Layout.fillHeight: true;

    clip: true;
}
