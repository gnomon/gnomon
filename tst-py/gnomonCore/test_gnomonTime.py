import unittest
import sys

from gnomon.core import gnomonTime

class TestGnomonTime(unittest.TestCase):
    '''
    Tests the gnomonTime class.
    '''

    def setUp(self):
        self.t0 = gnomonTime(0)
        self.t1 = gnomonTime(1)
        self.t2 = gnomonTime(0, gnomonTime.ArbitraryTime)
        self.t3 = gnomonTime(0, gnomonTime.DateTime)

    def tearDown(self):
        pass

    def test_gnomonTime_mode(self):
        assert self.t0.getMode() == gnomonTime.ArbitraryTime
        assert self.t0.getMode() == self.t2.getMode()
        assert self.t2.getMode() != self.t3.getMode()

    def test_gnomonTime_timestamp(self):
        assert self.t0.getTimeStamp() == 0
        assert self.t1.getTimeStamp() == 1
        assert self.t0.getTimeStamp() < self.t1.getTimeStamp()

#
# test_gnomonTime.py ends here.